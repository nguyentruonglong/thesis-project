import math
import collections
import json
import os
import numpy as np
from gensim.models import KeyedVectors
import matplotlib.pyplot as plt

def load_model_x_y(logpath):
    """
    Lấy các điểm x, y dưới dạng array
    vd:
    x_data = [0, 1000, 2000, 3000]
    y_data = [0, 0, 0.23, 0.25]
    """
    fp = open(logpath)
    x_step = 0
    x_data = []
    y_data = []
    """ Mở file training log rồi lấy từng điểm Dev BLEU cho vào array, 
        file json tensorboard tui kiểm tra thì nó lưu lung tung mặc dù dùng chung code nên ko dùng
        greedy cbow chỉ lưu những điểm epoch 15
        beam cbow chỉ lưu những điểm epoch 10 trở đi
    """	
    for i, line in enumerate(fp, 1):
        if i % 10 == 0:
            y_data.append(float(line.split()[15]))		
            if i % 80 == 0:
                x_step = x_step + 933			
                x_data.append(x_step)
            else:
                x_step = x_step + 1000	
                x_data.append(x_step)			
    fp.close()
    return x_data, y_data

def plot_random_vs_word2vec_greedy_models(word2vec):
    plt.figure(figsize=(10,10))
    x_greedy_random, y_greedy_random = load_model_x_y("experiments/seq2seq/greedy_search/gaussian_random/standard_deviation_1.0/training_log.txt")	
    x_ws_2, y_ws_2 = load_model_x_y("experiments/seq2seq/greedy_search/" + word2vec + "/window_size_2/training_log.txt")
    x_ws_5, y_ws_5 = load_model_x_y("experiments/seq2seq/greedy_search/" + word2vec + "/window_size_5/training_log.txt")
    x_ws_8, y_ws_8 = load_model_x_y("experiments/seq2seq/greedy_search/" + word2vec + "/window_size_8/training_log.txt")
	
    if word2vec == "cbow":	
        word2vec_label = "CBOW" 	
        plot_name = "GreedyCBOWBLEUchart.png"		
    elif word2vec == "skip_gram":	
        word2vec_label = "Skip-Gram" 	
        plot_name = "GreedySGBLEUchart.png"		
	
    plt.plot(x_greedy_random, y_greedy_random, label= "Greedy + Gaussian Random")
    plt.plot(x_ws_2, y_ws_2, label= "Greedy + " + word2vec_label + " + Window Size: 2")
    plt.plot(x_ws_5, y_ws_5, label= "Greedy + " + word2vec_label + " + Window Size: 5")
    plt.plot(x_ws_8, y_ws_8, label= "Greedy + " + word2vec_label + " + Window Size: 8")	
	
    plt.xlabel('Steps')
    plt.ylabel('Dev BLEU')

    plt.axis([0, 120000, 00.00, 0.30])

    plt.legend()

    #plt.show()		
    
    plt.savefig(plot_name) 	

def plot_random_vs_word2vec_beam_models(word2vec):
    plt.figure(figsize=(10,10))
    x_beam_random, y_beam_random = load_model_x_y("experiments/seq2seq/beam_search/gaussian_random/standard_deviation_1.0/training_log.txt")		
    x_ws_2, y_ws_2 = load_model_x_y("experiments/seq2seq/beam_search/" + word2vec + "/window_size_2/training_log.txt")
    x_ws_5, y_ws_5 = load_model_x_y("experiments/seq2seq/beam_search/" + word2vec + "/window_size_5/training_log.txt")
    x_ws_8, y_ws_8 = load_model_x_y("experiments/seq2seq/beam_search/" + word2vec + "/window_size_8/training_log.txt")

    if word2vec == "cbow":	
        word2vec_label = "CBOW" 	
        plot_name = "BeamCBOWBLEUchart.png"		
    elif word2vec == "skip_gram":	
        word2vec_label = "Skip-Gram" 	
        plot_name = "BeamSGBLEUchart.png"		
	
    plt.plot(x_beam_random, y_beam_random, label= "Beam + Gaussian Random")	
    plt.plot(x_ws_2, y_ws_2, label= "Beam + " + word2vec_label + " + Window Size: 2")
    plt.plot(x_ws_5, y_ws_5, label= "Beam + " + word2vec_label + " + Window Size: 5")
    plt.plot(x_ws_8, y_ws_8, label= "Beam + " + word2vec_label + " + Window Size: 8")	
	
    plt.xlabel('Steps')
    plt.ylabel('Dev BLEU')

    plt.axis([0, 120000, 00.00, 0.30])

    plt.legend()

    #plt.show()		
    
    plt.savefig(plot_name) 	
	
#plot_random_vs_word2vec_greedy_models("cbow")
#plot_random_vs_word2vec_greedy_models("skip_gram")	
#plot_random_vs_word2vec_beam_models("cbow")
#plot_random_vs_word2vec_beam_models("skip_gram")