# https://lxml.de/tutorial.html#elements-contain-text
# https://lxml.de/api/lxml.etree.XMLParser-class.html
# https://lxml.de/api/lxml.etree._Element-class.html
# https://cloud.google.com/translate/automl/docs/prepare

import os
from lxml import etree
import html
from pycorenlp import StanfordCoreNLP
import re
from pprint import pprint
import unicodedata

def load_txt_file_into_list(pathfile):
    txt_file = open(pathfile, 'rt', encoding="utf-8")
    raw_text = txt_file.read().replace("\r", "\n").strip()
    list_of_lines = list(raw_text.split("\n\n"))
    txt_file.close()
    return list_of_lines

def check_balanced_brackets(array_of_string):
    opening_brackets = {"(","[","{","<","``","`"}
    closing_brackets = {")","]","}",">","''","'"}
    pair_of_brackets = {('(',')'),('[',']'),('{','}'),("``","''"),("`","'"),("<",">")}
    stack = []
    mark_brackets = []
    for i in range(len(array_of_string)):
        if array_of_string[i] in opening_brackets or array_of_string[i] in closing_brackets:
            bracket = array_of_string[i]
            if bracket in opening_brackets:
                stack.append({"bracket": bracket, "position": i})
            elif bracket in closing_brackets:
                if len(stack) == 0:
                    mark_brackets.append({"bracket": bracket, "position": i})
                elif (stack[-1]["bracket"], bracket) not in pair_of_brackets:
                    mark_brackets.append({"bracket": bracket, "position": i})
                elif (stack[-1]["bracket"], bracket) in pair_of_brackets:
                    stack.pop()
    return stack + mark_brackets

def tokenize_sentence(paragraph, language):
    sentence_list = list()
    nlp = StanfordCoreNLP('http://localhost:9000') if language == "en" else StanfordCoreNLP('http://localhost:8000')
    annotated = nlp.annotate(paragraph, {'annotators': 'ssplit', 'outputFormat': 'json'})
    for sentence_object in annotated['sentences']:
        sentence = list()
        raw_sentence = [obj['word'].replace("-- --","–").replace("--", "–").replace("– –", "–").replace("-LRB-","(").replace("-LCB-","{").replace("-LSB-","[").replace("-RRB-",")").replace("-RCB-","}").replace("-RSB-","]") for obj in sentence_object['tokens']]

        tmp_set = {e['position'] for e in check_balanced_brackets(raw_sentence)}
        if len(raw_sentence) > 0  and len(tmp_set) > 0:
            for ith in range(len(raw_sentence)):
                word = raw_sentence[ith]
                if ith not in tmp_set and word != "":
                    sentence.append(word)
        else:
            sentence = raw_sentence

        sentence = ' '.join(sentence).replace("``","\"").replace("''","\"").replace("`","'").strip()
        sentence = re.sub(r'^([\s]*(([0-9]{1,2}|[a-zA-Z])[\s]+)?[\.\-–—)][\s]*)', "", sentence).strip()
        sentence = re.sub(r'(([\s])[\s]{1,})', " ", sentence).strip()
        sentence_list.append(sentence)

    return sentence_list

def normalize_qt21_data():
    en_vi_pairs = list()
    list_of_en_lines = load_txt_file_into_list('../../data/tmp/normalized_data_formats/qt21_data.en')
    list_of_vi_lines = load_txt_file_into_list('../../data/tmp/normalized_data_formats/qt21_data.vi')

    i = 0
    with open('../../data/tmp/normalized_data/qt21_data.en', 'w', encoding="utf8") as en_file, open('../../data/tmp/normalized_data/qt21_data.vi', 'w', encoding="utf8") as vi_file:
        if len(list_of_en_lines) == len(list_of_vi_lines):
            en_vi_pairs = list(zip(list_of_en_lines, list_of_vi_lines))
            if len(en_vi_pairs) > 0:
                for en_vi_pair in en_vi_pairs:
                    try:
                        en_text = unicodedata.normalize("NFC", en_vi_pair[0])
                        vi_text = unicodedata.normalize("NFC", en_vi_pair[1])
                        en_sentence_list = tokenize_sentence(html.unescape(en_text.strip()).replace("n 't ", "n't "), "en")
                        vi_sentence_list = tokenize_sentence(html.unescape(vi_text.strip()), "vi")
                        if len(en_sentence_list) > 0 and len(en_sentence_list) == len(vi_sentence_list):
                            for ith_pair in range(len(en_sentence_list)):
                                en_sentence = html.unescape(en_sentence_list[ith_pair])
                                vi_sentence = html.unescape(vi_sentence_list[ith_pair])
                                #if en_sentence != "" and vi_sentence != "" and en_sentence != vi_sentence and u"\u0301" not in vi_sentence and u"\u0309" not in vi_sentence and u"\u0303" not in vi_sentence and u"\u0300" not in vi_sentence and u"\u0323" not in vi_sentence:
                                '''Thay thế các ký tự mà không phải là whitespace character, alphanumeric character, ký tự en dash, em dash, punctuation character,
                                                    các ký tự mà không phải là các ký tự unicode trong Tiếng Việt (u00C0-u1EF9) thành whitespace character'''
                                en_sentence = re.sub(
                                        r'[^\w\s!"#$%&\'()*+,-.\/:;<=>?@\[\\\]^_`{|}~–—ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ]+',
                                        ' ', en_sentence)

                                '''Loại bỏ các whitespace character liên tục nhau thành chỉ còn một whitespace character'''
                                en_sentence = re.sub(r'(([\s])[\s]{1,})', ' ', en_sentence).strip()
                                vi_sentence = re.sub(
                                        r'[^\w\s!"#$%&\'()*+,-.\/:;<=>?@\[\\\]^_`{|}~–—ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ]+',
                                        ' ', vi_sentence)
                                vi_sentence = re.sub(r'(([\s])[\s]{1,})', ' ', vi_sentence).strip()

                                if "=" not in en_sentence and "=" not in vi_sentence and "<" not in en_sentence and "<" not in vi_sentence and ">" not in en_sentence and ">" not in vi_sentence and "@" not in en_sentence and "@" not in vi_sentence and "^" not in en_sentence and "^" not in vi_sentence and "+" not in en_sentence and "+" not in vi_sentence and "*" not in en_sentence and "*" not in vi_sentence and "~" not in en_sentence and "~" not in vi_sentence and "#" not in en_sentence and "#" not in vi_sentence and en_sentence != "" and vi_sentence != "" and en_sentence != vi_sentence:
                                    i += 1
                                    en_file.write(en_sentence + "\r\n")
                                    vi_file.write(vi_sentence + "\r\n")
                                    print("ID: " + str(i))
                                    print("EN: " + en_sentence)
                                    print("VI: " + vi_sentence)
                                    print("#############################################")

                    except Exception:
                        print("#############################################")

    en_file.close()
    vi_file.close()

normalize_qt21_data()

