import re
import unicodedata

def split_text_into_tokens(text):
	return list(text.split())

def load_txt_file_into_list(pathfile):
    txt_file = open(pathfile, 'rt', encoding="utf-8")
    raw_text = txt_file.read().replace("\r", "\n").strip()
    list_of_lines = list(raw_text.split("\n\n"))
    txt_file.close()
    return list_of_lines

def merge_all_txt_file_into_one():
    ev_sentence_pairs = {}
    iwslt13 = iwslt12 = iwslt15 = m = t = 0

    en_iwslt13_list = load_txt_file_into_list('../../data/tmp/normalized_data/iwslt13_data.en')
    vi_iwslt13_list = load_txt_file_into_list('../../data/tmp/normalized_data/iwslt13_data.vi')

    with open('../../data/processed_data/iwslt13_data.en', 'w', encoding="utf8") as iwslt13_en_file, open(
            '../../data/processed_data/iwslt13_data.vi', 'w', encoding="utf8") as iwslt13_vi_file:
        if len(en_iwslt13_list) == len(vi_iwslt13_list):
            tmp_pairs = list(zip(en_iwslt13_list, vi_iwslt13_list))
            ev_pair = {"en_sent": "", "vi_sent": ""}
            for tmp_pair in tmp_pairs:
                if len(split_text_into_tokens(tmp_pair[0])) <= 100 and len(split_text_into_tokens(tmp_pair[1])) <= 100:
                    iwslt13 += 1
                    iwslt13_en_file.write(unicodedata.normalize("NFC", tmp_pair[0]) + "\r\n")
                    iwslt13_vi_file.write(unicodedata.normalize("NFC", tmp_pair[1]) + "\r\n")

                    print("ID: " + str(iwslt13))
                    print("EN: " + tmp_pair[0])
                    print("VI: " + tmp_pair[1])
                    print("#############################################")

    iwslt13_en_file.close()
    iwslt13_vi_file.close()

    en_iwslt12_list = load_txt_file_into_list('../../data/tmp/normalized_data/iwslt12_data.en')
    vi_iwslt12_list = load_txt_file_into_list('../../data/tmp/normalized_data/iwslt12_data.vi')

    with open('../../data/processed_data/iwslt12_data.en', 'w', encoding="utf8") as iwslt12_en_file, open(
            '../../data/processed_data/iwslt12_data.vi', 'w', encoding="utf8") as iwslt12_vi_file:
        if len(en_iwslt12_list) == len(vi_iwslt12_list):
            tmp_pairs = list(zip(en_iwslt12_list, vi_iwslt12_list))
            ev_pair = {"en_sent": "", "vi_sent": ""}
            for tmp_pair in tmp_pairs:
                if tmp_pair[0] not in en_iwslt13_list and len(split_text_into_tokens(tmp_pair[0])) <= 100 and len(
                        split_text_into_tokens(tmp_pair[1])) <= 100:
                    iwslt12 += 1
                    iwslt12_en_file.write(unicodedata.normalize("NFC", tmp_pair[0]) + "\r\n")
                    iwslt12_vi_file.write(unicodedata.normalize("NFC", tmp_pair[1]) + "\r\n")

                    print("ID: " + str(iwslt12))
                    print("EN: " + tmp_pair[0])
                    print("VI: " + tmp_pair[1])
                    print("#############################################")

    iwslt12_en_file.close()
    iwslt12_vi_file.close()

    en_iwslt15_list = load_txt_file_into_list('../../data/tmp/normalized_data/iwslt15_data.en')
    vi_iwslt15_list = load_txt_file_into_list('../../data/tmp/normalized_data/iwslt15_data.vi')

    with open('../../data/processed_data/iwslt15_data.en', 'w', encoding="utf8") as iwslt15_en_file, open('../../data/processed_data/iwslt15_data.vi', 'w', encoding="utf8") as iwslt15_vi_file:
        if len(en_iwslt15_list) == len(vi_iwslt15_list):
            tmp_pairs = list(zip(en_iwslt15_list, vi_iwslt15_list))
            ev_pair = {"en_sent": "", "vi_sent": ""}
            for tmp_pair in tmp_pairs:
                if tmp_pair[0] not in en_iwslt13_list and tmp_pair[0] not in en_iwslt12_list and len(split_text_into_tokens(tmp_pair[0])) <= 100 and len(split_text_into_tokens(tmp_pair[1])) <= 100:
                    iwslt15 += 1
                    iwslt15_en_file.write(unicodedata.normalize("NFC", tmp_pair[0]) + "\r\n")
                    iwslt15_vi_file.write(unicodedata.normalize("NFC", tmp_pair[1]) + "\r\n")

                    print("ID: " + str(iwslt15))
                    print("EN: " + tmp_pair[0])
                    print("VI: " + tmp_pair[1])
                    print("#############################################")

    iwslt15_en_file.close()
    iwslt15_vi_file.close()

    for filename in [("qt21_data.en", "qt21_data.vi"), ("iwslt15_data.en", "iwslt15_data.vi"), ("evbnews_data.en", "evbnews_data.vi")]:
        en_file_path = '../../data/tmp/normalized_data/' + filename[0]
        vi_file_path = '../../data/tmp/normalized_data/' + filename[1]

        en_sentence_list = load_txt_file_into_list(en_file_path)
        vi_sentence_list = load_txt_file_into_list(vi_file_path)

        if len(en_sentence_list) == len(vi_sentence_list):
            tmp_pairs = list(zip(en_sentence_list, vi_sentence_list))
            ev_pair = {"en_sent": "", "vi_sent": ""}
            for tmp_pair in tmp_pairs:
                if tmp_pair[0] not in en_iwslt13_list and tmp_pair[0] not in en_iwslt12_list and len(split_text_into_tokens(tmp_pair[0])) <= 100 and len(split_text_into_tokens(tmp_pair[1])) <= 100:
                    ev_pair["en_sent"] = unicodedata.normalize("NFC", tmp_pair[0])
                    ev_pair["vi_sent"] = unicodedata.normalize("NFC", tmp_pair[1])

                    if ev_pair["en_sent"] != "" and ev_pair["vi_sent"] != "" and ev_pair["en_sent"] != ev_pair["vi_sent"]:
                        '''Thêm các cặp câu được duyệt qua vào một Dictionary bao gồm key là câu tiếng Anh và value là bản dịch câu tiếng Việt tương ứng để loại bỏ các câu tiếng Anh trung nhau'''
                        ev_sentence_pairs.update({ev_pair["en_sent"] : ev_pair["vi_sent"]})
                        t += 1

    with open('../../data/processed_data/merged_data.en', 'w', encoding="utf8") as merged_en_file, open('../../data/processed_data/merged_data.vi', 'w', encoding="utf8") as merged_vi_file:
        for (en_sentence, vi_sentence) in ev_sentence_pairs.items():
            merged_en_file.write(unicodedata.normalize("NFC", en_sentence) + "\r\n")
            merged_vi_file.write(unicodedata.normalize("NFC", vi_sentence) + "\r\n")
            m += 1
            print("ID: " + str(m))
            print("EN: " + en_sentence)
            print("VI: " + vi_sentence)
            print("#############################################")

    merged_en_file.close()
    merged_vi_file.close()
    print(t)
    print(m)
    print(iwslt13)
    print(iwslt12)
    print(iwslt15)


merge_all_txt_file_into_one()