import re
import os
import html
from lxml import etree
from pycorenlp import StanfordCoreNLP
from importlib.machinery import SourceFileLoader
TMXFileIO = SourceFileLoader(fullname = 'TMXFileIO', path = "..\\..\\build_dataset\\tmxio\\tmx_file_io.py").load_module()


def read_qt21_data_into_txt_files():
    i = 0
    ev_sentence_pairs = TMXFileIO.read_data_from_tmx_file('../../data/raw_data/qt21_envi/qt21_envi.tmx')
    with open('../../data/tmp/normalized_data_formats/qt21_data.en', 'w', encoding="utf8") as en_file, open('../../data/tmp/normalized_data_formats/qt21_data.vi', 'w', encoding="utf8") as vi_file:
        for ev_sentence_pair in ev_sentence_pairs:
            i += 1
            en_sentence = ev_sentence_pair["en_sent"]
            vi_sentence = ev_sentence_pair["vi_sent"]
            en_file.write(en_sentence + "\r\n")
            vi_file.write(vi_sentence + "\r\n")
            print("ID: " + str(i))
            print("EN: " + en_sentence)
            print("VI: " + vi_sentence)
            print("#############################################")

    en_file.close()
    vi_file.close()

#read_qt21_data_into_txt_files()

def get_all_filepaths(directory):
    filename_list = os.listdir(directory)
    all_filepaths = list()
    for filename in filename_list:
        filepath = os.path.join(directory, filename)
        if not os.path.isdir(filepath):
            all_filepaths.append(filepath)
    return all_filepaths

def read_evbnews_data_into_txt_files():
    i = 0
    ev_sentence_pairs = list()
    parser = etree.XMLParser(recover=True)  # cố gắng phân tích cú pháp của một file XML bị hỏng

    for filepath in get_all_filepaths('../../data/raw_data/evb_corpus/'):
        sgml_file = open(filepath, 'rt', encoding="utf8")
        xml_content = html.unescape(sgml_file.read()).replace("&","&amp; ")  # đọc toàn bộ tập tin và trả về một biến có kiểu string
        sgml_file.close()

        root = etree.fromstring(xml_content, parser=parser)
        for spair_tag in root.iter("spair"):
            s_tag_list = spair_tag.getchildren()
            if len(s_tag_list) == 2:
                ev_pair = {"en_sent": "", "vi_sent": ""}
                for s_tag in s_tag_list:
                    if "en" in s_tag.attrib['id']:
                        ev_pair["en_sent"] = s_tag.text.strip()
                    elif "vn" in s_tag.attrib['id']:
                        ev_pair["vi_sent"] = s_tag.text.strip()

                ev_sentence_pairs.append(ev_pair)
                
   
    with open('../../data/tmp/normalized_data_formats/evbnews_data.en', 'w', encoding="utf8") as en_file, open('../../data/tmp/normalized_data_formats/evbnews_data.vi', 'w', encoding="utf8") as vi_file:
        for ev_sentence_pair in ev_sentence_pairs:
            i += 1
            en_sentence = ev_sentence_pair["en_sent"]
            vi_sentence = ev_sentence_pair["vi_sent"]
            en_file.write(en_sentence + "\r\n")
            vi_file.write(vi_sentence + "\r\n")
            print("ID: " + str(i))
            print("EN: " + en_sentence)
            print("VI: " + vi_sentence)
            print("#############################################")

    en_file.close()
    vi_file.close()

#read_evbnews_data_into_txt_files()

def read_cepnews_data_into_txt_files():
    i = 0
    ev_pairs = list()
    parser = etree.XMLParser(recover=True)  # try hard to parse through broken XML
    cepnews_file = open('../../data/raw_data/cepnews_envi/cepnews_data.txt', 'rt', encoding="utf8")  # open file qt21_prepared_envi.tmx for reading text data
    cepnews_data = html.unescape(cepnews_file.read()).replace("&","&amp; ")  # read the entire file into a string variable
    cepnews_data = "<rawdata>" + cepnews_data + "</rawdata>"
    cepnews_file.close()    # close the file
    root = etree.fromstring(cepnews_data, parser=parser)

    for article_tag in root.iter("article"):
        for p_tag in article_tag.getchildren():
            ev_pair = {"en_text": "", "vi_text": ""}
            for span_tag in p_tag.getchildren():
                if span_tag.items()[0][1] == "en":
                    ev_pair["en_text"] = span_tag.text
                elif span_tag.items()[0][1] == "vi":
                    ev_pair["vi_text"] = span_tag.text
            
            if ev_pair["en_text"] != "" and ev_pair["vi_text"] != "" and "\n" not in ev_pair["en_text"] and "\n" not in ev_pair["vi_text"] and "\r" not in ev_pair["en_text"] and "\r" not in ev_pair["vi_text"]:
                ev_pairs.append(ev_pair)
    
    with open('../../data/tmp/normalized_data_formats/cepnews_data.en', 'w', encoding="utf8") as en_file, open('../../data/tmp/normalized_data_formats/cepnews_data.vi', 'w', encoding="utf8") as vi_file:
        for ev_pair in ev_pairs:
            i += 1
            en_text = ev_pair["en_text"]
            vi_text = ev_pair["vi_text"]
            en_file.write(en_text + "\r\n")
            vi_file.write(vi_text + "\r\n")
            print("ID: " + str(i))
            print("EN: " + en_text)
            print("VI: " + vi_text)
            print("#############################################")

    en_file.close()
    vi_file.close()

#read_cepnews_data_into_txt_files()

def read_cnn_data_into_txt_file():
    i = 0
    with open('../../data/tmp/normalized_data_formats/cnn_content_data.txt', 'w', encoding="utf8") as cnn_content_data:
        for filepath in get_all_filepaths('../../data/raw_data/cnn_data/'):
            txt_file = open(filepath, 'rt', encoding="utf8")
            article_content = html.unescape(txt_file.read().strip())
            txt_file.close()
            i += 1
            cnn_content_data.write(article_content + "\n")
            print("ID: " + str(i))
            print("#############################################")

    cnn_content_data.close()

#read_cnn_data_into_txt_file()

def read_baomoi_data_into_txt_file():
    i = 0
    with open('../../data/tmp/normalized_data_formats/baomoi_content_data.txt', 'w', encoding="utf8") as baomoi_content_data:
        for filepath in get_all_filepaths('../../data/raw_data/baomoi_data/'):
            txt_file = open(filepath, 'rt', encoding="utf8")
            article_content = html.unescape(txt_file.read().replace("\n", " ").strip())
            txt_file.close()
            i += 1
            baomoi_content_data.write(article_content + "\n")
            print("ID: " + str(i))
            print("#############################################")

    baomoi_content_data.close()

read_baomoi_data_into_txt_file()