import html
import re
from pycorenlp import StanfordCoreNLP
import unicodedata

def load_txt_file_into_list(pathfile):
    txt_file = open(pathfile, 'rt', encoding="utf-8")
    raw_text = unicodedata.normalize("NFC", txt_file.read().replace("\r", "\n").strip())
    list_of_lines = list(raw_text.split("\n\n"))
    txt_file.close()
    return list_of_lines

def check_balanced_brackets(array_of_string):
    opening_brackets = {"(","[","{","<","``","`"}
    closing_brackets = {")","]","}",">","''","'"}
    pair_of_brackets = {('(',')'),('[',']'),('{','}'),("``","''"),("`","'"),("<",">")}
    stack = []
    mark_brackets = []
    for i in range(len(array_of_string)):
        if array_of_string[i] in opening_brackets or array_of_string[i] in closing_brackets:
            bracket = array_of_string[i]
            if bracket in opening_brackets:
                stack.append({"bracket": bracket, "position": i})
            elif bracket in closing_brackets:
                if len(stack) == 0:
                    mark_brackets.append({"bracket": bracket, "position": i})
                elif (stack[-1]["bracket"], bracket) not in pair_of_brackets:
                    mark_brackets.append({"bracket": bracket, "position": i})
                elif (stack[-1]["bracket"], bracket) in pair_of_brackets:
                    stack.pop()
    return stack + mark_brackets

def tokenize_sentence(paragraph):
    sentence_list = list()
    '''Loại bỏ các chuỗi mà là chú thích hình ảnh như (Photo: Getty Images Europe), (Photo: i-Images Picture Agency), (GREG BAKER/AFP/Getty Images), (Johannes Eisele/AFP/Getty Images), (Ted S. Warren-Pool/Getty Images), (Ảnh của Matthew Lewis/Getty Images), (Photo by Matthew Lewis/Getty Images), (Getty Images/iStockphoto), [Getty Images], (Dennis Brack-Pool/Getty Images), (Cancan Chu/Getty Images), (Image: Moment RF), (Ảnh: Moment RF), (Image: Navy photo by Petty Officer 2nd Class Robert Houlihan)'''
    if re.compile(r'(\(Ảnh:)|(\(ảnh:)|(\(photo:)|(\(Photo:)|(\(photos:)|(\(Photos:)|(Ảnh:)(ảnh:)|(Photo:)|(\(Getty Images)|(Getty Images)').search(paragraph) is not None:
        return []
    paragraph = paragraph.replace("’","'").replace("‘","'").replace("“",'"').replace("”",'"').replace(u'\xa0', u' ')
    nlp = StanfordCoreNLP('http://localhost:9000')
    annotated = nlp.annotate(paragraph, {'annotators': 'ssplit', 'outputFormat': 'json'})
    for sentence_object in annotated['sentences']:
        sentence = list()
        raw_sentence = [obj['word'].replace("--", "–").replace("-LRB-","(").replace("-LCB-","{").replace("-LSB-","[").replace("-RRB-",")").replace("-RCB-","}").replace("-RSB-","]") for obj in sentence_object['tokens']]   # thay chuoi "--" thanh ky tu EN DASH
        tmp_set = {e['position'] for e in check_balanced_brackets(raw_sentence)}
        if len(raw_sentence) > 0  and len(tmp_set) > 0:
            for ith in range(len(raw_sentence)):
                if ith not in tmp_set and raw_sentence[ith] != "":
                    sentence.append(raw_sentence[ith])
        else:
            sentence = raw_sentence
        
        
        sentence = ' '.join(sentence).replace("``","\"").replace("''","\"").replace("`","'").strip()
        #print(sentence)
        sentence = re.sub(r'^([\(])[^\(\)]*([\)])$', "", sentence).strip()
        #print(sentence)
        sentence = re.sub(r'^([\(]|[\[])([^\(\[\)\]\/])+([\]]|[\)])$', "", sentence).strip()
        #print(sentence)
        sentence = re.sub(r'([\(]|[\[])(([^\(\[\)\]\:\/])+([\/]|[\:]))+([^\(\[\)\]\:\/])+([\]]|[\)])', "", sentence).strip()
        #print(sentence)
        sentence = re.sub(r'^([\s]*(([0-9]{1,2}|[a-zA-Z])[\s]+)?[\.\-–—)][\s]*)', "", sentence).strip()
        #print(sentence)
        sentence = re.sub(r'(([\s])[\s]{1,})', ' ', sentence).strip()
        #print(sentence)
        sentence_list.append(sentence)

    return sentence_list


def normalize_cepnews_data():
    en_vi_pairs = list()

    list_of_en_lines = load_txt_file_into_list('../../data/tmp/normalized_data_formats/cepnews_data.en')
    list_of_vi_lines = load_txt_file_into_list('../../data/tmp/normalized_data_formats/cepnews_data.vi')

    i = 0
    with open('../../data/tmp/normalized_data/cepnews_data.en', 'w', encoding="utf8") as en_file, open('../../data/tmp/normalized_data/cepnews_data.vi', 'w', encoding="utf8") as vi_file:
        if len(list_of_en_lines) == len(list_of_vi_lines):
            en_vi_pairs = list(zip(list_of_en_lines, list_of_vi_lines))
            if len(en_vi_pairs) > 0:
                for en_vi_pair in en_vi_pairs:
                    en_text = en_vi_pair[0]
                    vi_text = en_vi_pair[1]

                    en_sentence_list = tokenize_sentence(html.unescape(en_text.strip()).replace("n 't ", "n't "))
                    vi_sentence_list = tokenize_sentence(html.unescape(vi_text.strip()))
                    if len(en_sentence_list) > 0 and len(en_sentence_list) == len(vi_sentence_list):
                        for ith_pair in range(len(en_sentence_list)):
                            en_sentence = html.unescape(en_sentence_list[ith_pair])
                            vi_sentence = html.unescape(vi_sentence_list[ith_pair])

                            '''Thay thế các ký tự mà không phải là whitespace character, alphanumeric character, ký tự en dash, em dash, punctuation character,
                                                                        các ký tự mà không phải là các ký tự unicode trong Tiếng Việt (u00C0-u1EF9) thành whitespace character'''
                            en_sentence = re.sub(
                                r'[^\w\s!"#$%&\'()*+,-.\/:;<=>?@\[\\\]^_`{|}~–—ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ]+',
                                ' ', en_sentence)

                            '''Loại bỏ các whitespace character liên tục nhau thành chỉ còn một whitespace character'''
                            en_sentence = re.sub(r'(([\s])[\s]{1,})', ' ', en_sentence).strip()

                            vi_sentence = re.sub(
                                r'[^\w\s!"#$%&\'()*+,-.\/:;<=>?@\[\\\]^_`{|}~–—ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ]+',
                                ' ', vi_sentence)
                            vi_sentence = re.sub(r'(([\s])[\s]{1,})', ' ', vi_sentence).strip()

                            if "=" not in en_sentence and "=" not in vi_sentence and "<" not in en_sentence and "<" not in vi_sentence and ">" not in en_sentence and ">" not in vi_sentence and "@" not in en_sentence and "@" not in vi_sentence and "^" not in en_sentence and "^" not in vi_sentence and "+" not in en_sentence and "+" not in vi_sentence and "*" not in en_sentence and "*" not in vi_sentence and "~" not in en_sentence and "~" not in vi_sentence and "#" not in en_sentence and "#" not in vi_sentence and en_sentence != "" and vi_sentence != "" and en_sentence != vi_sentence:
                                i += 1
                                en_file.write(en_sentence + "\r\n")
                                vi_file.write(vi_sentence + "\r\n")
                                print("ID: " + str(i))
                                print("EN: " + en_sentence)
                                print("VI: " + vi_sentence)
                                print("#############################################")

    en_file.close()
    vi_file.close()

normalize_cepnews_data()
