import os

def getListOfFiles(dirName):
    listOfFile = os.listdir(dirName)
    allFiles = list()
    for entry in listOfFile:
        fullPath = os.path.join(dirName, entry)
        if not os.path.isdir(fullPath):
            allFiles.append(fullPath)
    return allFiles

def load_txt_file_into_list(pathfile):
    txt_file = open(pathfile, 'rt', encoding="utf-8")
    raw_text = txt_file.read().replace("\r", "\n").strip()
    list_of_lines = list(raw_text.split("\n\n"))
    txt_file.close()
    return list_of_lines

def split_text_into_tokens(text):
	return list(text.split())

def statistics_for_corpus(folder_path):
    for file_path in getListOfFiles(folder_path):
        print("Corpus file :" + file_path)
        sentence_list = load_txt_file_into_list(file_path)
        num_sentences = len(sentence_list)
        print("Number of English sentences: " + str(num_sentences))
        tokenized_sentence_list = [split_text_into_tokens(sentence) for sentence in sentence_list]
        num_tokens = sum(len(x) for x in tokenized_sentence_list)
        print("Number of tokens: " + str(num_tokens))

if __name__ == '__main__':
    statistics_for_corpus("../../data/processed_data/")