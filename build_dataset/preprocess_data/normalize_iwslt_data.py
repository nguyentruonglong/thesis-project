import html
import re
from pycorenlp import StanfordCoreNLP
import pprint
import csv
from importlib.machinery import SourceFileLoader
import unicodedata

def load_txt_file_into_list(pathfile):
    txt_file = open(pathfile, 'rt', encoding="utf-8")
    raw_text = txt_file.read().replace("\r", "\n").strip()
    list_of_lines = list(raw_text.split("\n"))
    txt_file.close()
    return list_of_lines

def check_balanced_brackets(array_of_string):
    opening_brackets = {"(","[","{","<","``","`"}
    closing_brackets = {")","]","}",">","''","'"}
    pair_of_brackets = {('(',')'),('[',']'),('{','}'),("``","''"),("`","'"),("<",">")}
    stack = []
    mark_brackets = []
    for i in range(len(array_of_string)):
        if array_of_string[i] in opening_brackets or array_of_string[i] in closing_brackets:
            bracket = array_of_string[i]
            if bracket in opening_brackets:
                stack.append({"bracket": bracket, "position": i})
            elif bracket in closing_brackets:
                if len(stack) == 0:
                    mark_brackets.append({"bracket": bracket, "position": i})
                elif (stack[-1]["bracket"], bracket) not in pair_of_brackets:
                    mark_brackets.append({"bracket": bracket, "position": i})
                elif (stack[-1]["bracket"], bracket) in pair_of_brackets:
                    stack.pop()
    return stack + mark_brackets

def tokenize_sentence(paragraph, language):
    sentence_list = list()
    paragraph = unicodedata.normalize("NFC", paragraph)
    nlp = StanfordCoreNLP('http://localhost:9000') if language == "en" else StanfordCoreNLP('http://localhost:8000')
    annotated = nlp.annotate(paragraph, {'annotators': 'ssplit', 'outputFormat': 'json'})
    for sentence_object in annotated['sentences']:
        sentence = list()
        raw_sentence = [obj['word'].replace("-- --","–").replace("--", "–").replace("– –", "–").replace("-LRB-","(").replace("-LCB-","{").replace("-LSB-","[").replace("-RRB-",")").replace("-RCB-","}").replace("-RSB-","]") for obj in sentence_object['tokens']]

        tmp_set = {e['position'] for e in check_balanced_brackets(raw_sentence)}
        if len(raw_sentence) > 0  and len(tmp_set) > 0:
            for ith in range(len(raw_sentence)):
                word = raw_sentence[ith]
                if ith not in tmp_set and word != "":
                    sentence.append(word)
        else:
            sentence = raw_sentence

        sentence = ' '.join(sentence).replace("``","\"").replace("''","\"").replace("`","'").strip()
        sentence = re.sub(r'^([\s]*(([0-9]{1,2}|[a-zA-Z])[\s]+)?[\.\-–—)][\s]*)', "", sentence).strip()
        sentence = re.sub(r'(([\s])[\s]{1,})', " ", sentence).strip()
        sentence_list.append(sentence)

    return sentence_list


def tokenize_word(paragraph, language):
    sentence = list()
    raw_sentence = list()
    sentence_list = list()
    paragraph = unicodedata.normalize("NFC", paragraph)
    nlp = StanfordCoreNLP('http://localhost:9000') if language == "en" else StanfordCoreNLP('http://localhost:8000')
    annotated = nlp.annotate(paragraph, {'annotators': 'tokenize', 'outputFormat': 'json'})
    for word_object in annotated['tokens']:
        if word_object['word'] != "":
            raw_sentence.append(
                word_object['word'].replace("--", "–").replace("-LRB-","(").replace("-LCB-","{").replace("-LSB-","[").replace("-RRB-",")").replace("-RCB-","}").replace("-RSB-","]"))

    tmp_set = {e['position'] for e in check_balanced_brackets(raw_sentence)}
    if len(raw_sentence) > 0 and len(tmp_set) > 0:
        for ith in range(len(raw_sentence)):
            if ith not in tmp_set and raw_sentence[ith] != "":
                sentence.append(raw_sentence[ith])
    else:
        sentence = raw_sentence

    sentence = ' '.join(sentence).replace("``", "\"").replace("''", "\"").replace("`", "'").strip()
    sentence = re.sub(r'^([\s]*(([0-9]{1,2}|[a-zA-Z])[\s]+)?[\.\-–—)][\s]*)', "", sentence).strip()
    sentence = re.sub(r'(([\s])[\s]{1,})', " ", sentence).strip()
    sentence_list.append(sentence)

    return sentence_list


def normalize_iwsltenvi_data():
    en_vi_pairs = list()

    list_of_en_lines = load_txt_file_into_list('../../data/raw_data/iwslt_envi/tst2013.en')
    list_of_vi_lines = load_txt_file_into_list('../../data/raw_data/iwslt_envi/tst2013.vi')

    iwslt13 = 0
    with open('../../data/tmp/normalized_data/iwslt13_data.en', 'w', encoding="utf8") as en_file, open(
            '../../data/tmp/normalized_data/iwslt13_data.vi', 'w', encoding="utf8") as vi_file:
        if len(list_of_en_lines) == len(list_of_vi_lines):
            en_vi_pairs = list(zip(list_of_en_lines, list_of_vi_lines))

        if len(en_vi_pairs) > 0:
            for en_vi_pair in en_vi_pairs:
                en_text = en_vi_pair[0]
                vi_text = en_vi_pair[1]
                en_text = html.unescape(
                    re.sub(r'(&amp;( amp ;)* (([^;\s])+) ;)', r'&\3;', en_text.strip()).replace("-- --", "–").replace(
                        "--", "–")).replace("– –", "–").replace("n 't ",
                                                                "n't ")  # loại bỏ câu bị lỗi chứa các ký tự đặc biệt, ví dụ câu chứa các chuỗi " &amp; lt ; em &amp; gt ; &amp; lt ; / em &amp; gt ;", &amp; amp ; amp ;", "&amp; amp ;", :"&amp; lt ;",..
                vi_text = html.unescape(
                    re.sub(r'(&amp;( amp ;)* (([^;\s])+) ;)', r'&\3;', vi_text.strip()).replace("-- --", "–").replace(
                        "--", "–")).replace("– –", "–")
                en_sentence_list = tokenize_word(
                    html.unescape(en_text.replace("< em >", " ").replace("< / em >", " ").strip()), "en")
                vi_sentence_list = tokenize_word(
                    html.unescape(vi_text.replace("< em >", " ").replace("< / em >", " ").strip()), "vi")

                if len(en_sentence_list) == len(vi_sentence_list) and len(en_sentence_list) > 0:
                    for ith_pair in range(len(en_sentence_list)):
                        en_sentence = en_sentence_list[ith_pair]
                        vi_sentence = vi_sentence_list[ith_pair]
                        en_sentence = re.sub(
                            r'[^\w\s!"#$%&\'()*+,-.\/:;<=>?@\[\\\]^_`{|}~–—ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ]+',
                            ' ', en_sentence)

                        '''Loại bỏ các whitespace character liên tục nhau thành chỉ còn một whitespace character'''
                        en_sentence = re.sub(r'(([\s])[\s]{1,})', ' ', en_sentence).strip()

                        vi_sentence = re.sub(
                            r'[^\w\s!"#$%&\'()*+,-.\/:;<=>?@\[\\\]^_`{|}~–—ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ]+',
                            ' ', vi_sentence)
                        vi_sentence = re.sub(r'(([\s])[\s]{1,})', ' ', vi_sentence).strip()

                        if "=" not in en_sentence and "=" not in vi_sentence and "<" not in en_sentence and "<" not in vi_sentence and ">" not in en_sentence and ">" not in vi_sentence and "@" not in en_sentence and "@" not in vi_sentence and "^" not in en_sentence and "^" not in vi_sentence and "+" not in en_sentence and "+" not in vi_sentence and "*" not in en_sentence and "*" not in vi_sentence and "~" not in en_sentence and "~" not in vi_sentence and "#" not in en_sentence and "#" not in vi_sentence and en_sentence != "" and vi_sentence != "" and en_sentence != vi_sentence:  # and re.search('[0-9]+[\.]|[\(]|[\)]', en_sentence) is None and re.search('[0-9]+[\.]|[\(]|[\)]', vi_sentence) is None:
                            iwslt13 += 1
                            en_file.write(en_sentence + "\r\n")
                            vi_file.write(vi_sentence + "\r\n")
                            print("ID: " + str(iwslt13))
                            print("EN: " + en_sentence)
                            print("VI: " + vi_sentence)
                            print("#############################################")

    en_file.close()
    vi_file.close()


    en_vi_pairs = list()

    list_of_en_lines = load_txt_file_into_list('../../data/raw_data/iwslt_envi/tst2012.en')
    list_of_vi_lines = load_txt_file_into_list('../../data/raw_data/iwslt_envi/tst2012.vi')


    iwslt12 = 0
    with open('../../data/tmp/normalized_data/iwslt12_data.en', 'w', encoding="utf8") as en_file, open(
            '../../data/tmp/normalized_data/iwslt12_data.vi', 'w', encoding="utf8") as vi_file:
        if len(list_of_en_lines) == len(list_of_vi_lines):
            en_vi_pairs = list(zip(list_of_en_lines, list_of_vi_lines))

        if len(en_vi_pairs) > 0:
            for en_vi_pair in en_vi_pairs:
                en_text = en_vi_pair[0].replace("\n", "")
                vi_text = en_vi_pair[1].replace("\n", "")
                en_text = html.unescape(
                    re.sub(r'(&amp;( amp ;)* (([^;\s])+) ;)', r'&\3;', en_text.strip()).replace("-- --", "–").replace(
                        "--", "–")).replace("– –", "–").replace("n 't ",
                                                                "n't ")  # loại bỏ câu bị lỗi chứa các ký tự đặc biệt, ví dụ câu chứa các chuỗi " &amp; lt ; em &amp; gt ; &amp; lt ; / em &amp; gt ;", &amp; amp ; amp ;", "&amp; amp ;", :"&amp; lt ;",..
                vi_text = html.unescape(
                    re.sub(r'(&amp;( amp ;)* (([^;\s])+) ;)', r'&\3;', vi_text.strip()).replace("-- --", "–").replace(
                        "--", "–")).replace("– –", "–")
                en_sentence_list = tokenize_word(
                    html.unescape(en_text.replace("< em >", " ").replace("< / em >", " ").strip()), "en")
                vi_sentence_list = tokenize_word(
                    html.unescape(vi_text.replace("< em >", " ").replace("< / em >", " ").strip()), "vi")

                if len(en_sentence_list) == len(vi_sentence_list) and len(en_sentence_list) > 0:
                    for ith_pair in range(len(en_sentence_list)):
                        en_sentence = en_sentence_list[ith_pair]
                        vi_sentence = vi_sentence_list[ith_pair]
                        en_sentence = re.sub(
                            r'[^\w\s!"#$%&\'()*+,-.\/:;<=>?@\[\\\]^_`{|}~–—ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ]+',
                            ' ', en_sentence)

                        '''Loại bỏ các whitespace character liên tục nhau thành chỉ còn một whitespace character'''
                        en_sentence = re.sub(r'(([\s])[\s]{1,})', ' ', en_sentence).strip()

                        vi_sentence = re.sub(
                            r'[^\w\s!"#$%&\'()*+,-.\/:;<=>?@\[\\\]^_`{|}~–—ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ]+',
                            ' ', vi_sentence)
                        vi_sentence = re.sub(r'(([\s])[\s]{1,})', ' ', vi_sentence).strip()

                        if "=" not in en_sentence and "=" not in vi_sentence and "<" not in en_sentence and "<" not in vi_sentence and ">" not in en_sentence and ">" not in vi_sentence and "@" not in en_sentence and "@" not in vi_sentence and "^" not in en_sentence and "^" not in vi_sentence and "+" not in en_sentence and "+" not in vi_sentence and "*" not in en_sentence and "*" not in vi_sentence and "~" not in en_sentence and "~" not in vi_sentence and "#" not in en_sentence and "#" not in vi_sentence and en_sentence != "" and vi_sentence != "" and en_sentence != vi_sentence:  # and re.search('[0-9]+[\.]|[\(]|[\)]', en_sentence) is None and re.search('[0-9]+[\.]|[\(]|[\)]', vi_sentence) is None:
                            iwslt12 += 1
                            en_file.write(en_sentence + "\r\n")
                            vi_file.write(vi_sentence + "\r\n")
                            print("ID: " + str(iwslt12))
                            print("EN: " + en_sentence)
                            print("VI: " + vi_sentence)
                            print("#############################################")

    en_file.close()
    vi_file.close()

    en_vi_pairs = list()

    list_of_en_lines = load_txt_file_into_list('../../data/raw_data/iwslt_envi/train.en')
    list_of_vi_lines = load_txt_file_into_list('../../data/raw_data/iwslt_envi/train.vi')

    iwslt15 = 0
    with open('../../data/tmp/normalized_data/iwslt15_data.en', 'w', encoding="utf8") as en_file, open(
            '../../data/tmp/normalized_data/iwslt15_data.vi', 'w', encoding="utf8") as vi_file:
        if len(list_of_en_lines) == len(list_of_vi_lines):
            en_vi_pairs = list(zip(list_of_en_lines, list_of_vi_lines))

        if len(en_vi_pairs) > 0:
            for en_vi_pair in en_vi_pairs:
                en_text = en_vi_pair[0]
                vi_text = en_vi_pair[1]
                en_text = html.unescape(
                    re.sub(r'(&amp;( amp ;)* (([^;\s])+) ;)', r'&\3;', en_text.strip()).replace("-- --", "–").replace(
                        "--", "–")).replace("– –", "–").replace("n 't ",
                                                                "n't ")  # loại bỏ câu bị lỗi chứa các ký tự đặc biệt, ví dụ câu chứa các chuỗi " &amp; lt ; em &amp; gt ; &amp; lt ; / em &amp; gt ;", &amp; amp ; amp ;", "&amp; amp ;", :"&amp; lt ;",..
                vi_text = html.unescape(
                    re.sub(r'(&amp;( amp ;)* (([^;\s])+) ;)', r'&\3;', vi_text.strip()).replace("-- --", "–").replace(
                        "--", "–")).replace("– –", "–")
                en_sentence_list = tokenize_sentence(
                    html.unescape(en_text.replace("< em >", " ").replace("< / em >", " ").strip()), "en")
                vi_sentence_list = tokenize_sentence(
                    html.unescape(vi_text.replace("< em >", " ").replace("< / em >", " ").strip()), "vi")

                if len(en_sentence_list) == len(vi_sentence_list) and len(en_sentence_list) > 0:
                    for ith_pair in range(len(en_sentence_list)):
                        en_sentence = en_sentence_list[ith_pair]
                        vi_sentence = vi_sentence_list[ith_pair]
                        '''Thay thế các ký tự mà không phải là whitespace character, alphanumeric character, ký tự en dash, em dash, punctuation character,
                                            các ký tự mà không phải là các ký tự unicode trong Tiếng Việt (u00C0-u1EF9) thành whitespace character'''
                        en_sentence = re.sub(
                            r'[^\w\s!"#$%&\'()*+,-.\/:;<=>?@\[\\\]^_`{|}~–—ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ]+',
                            ' ', en_sentence)

                        '''Loại bỏ các whitespace character liên tục nhau thành chỉ còn một whitespace character'''
                        en_sentence = re.sub(r'(([\s])[\s]{1,})', ' ', en_sentence).strip()

                        vi_sentence = re.sub(
                            r'[^\w\s!"#$%&\'()*+,-.\/:;<=>?@\[\\\]^_`{|}~–—ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ]+',
                            ' ', vi_sentence)
                        vi_sentence = re.sub(r'(([\s])[\s]{1,})', ' ', vi_sentence).strip()

                        if "=" not in en_sentence and "=" not in vi_sentence and "<" not in en_sentence and "<" not in vi_sentence and ">" not in en_sentence and ">" not in vi_sentence and "@" not in en_sentence and "@" not in vi_sentence and "^" not in en_sentence and "^" not in vi_sentence and "+" not in en_sentence and "+" not in vi_sentence and "*" not in en_sentence and "*" not in vi_sentence and "~" not in en_sentence and "~" not in vi_sentence and "#" not in en_sentence and "#" not in vi_sentence and en_sentence != "" and vi_sentence != "" and en_sentence != vi_sentence:  # and re.search('[0-9]+[\.]|[\(]|[\)]', en_sentence) is None and re.search('[0-9]+[\.]|[\(]|[\)]', vi_sentence) is None:
                            iwslt15 += 1
                            en_file.write(en_sentence + "\r\n")
                            vi_file.write(vi_sentence + "\r\n")
                            print("ID: " + str(iwslt15))
                            print("EN: " + en_sentence)
                            print("VI: " + vi_sentence)
                            print("#############################################")

    en_file.close()
    vi_file.close()
    print(iwslt13)
    print(iwslt12)
    print(iwslt15)

normalize_iwsltenvi_data()
    