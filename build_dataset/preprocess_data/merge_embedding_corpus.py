from collections import Counter
import itertools

def split_text_into_tokens(text):
	return list(text.split())

def load_txt_file_into_list(pathfile):
    txt_file = open(pathfile, 'rt', encoding="utf-8")
    raw_text = txt_file.read().replace("\r", "\n").strip()
    list_of_lines = list(raw_text.split("\n\n"))
    txt_file.close()
    return list_of_lines

def load_text_corpus(language):
    if language == "en":
        training_sentence_list = load_txt_file_into_list('../../data/processed_data/merged_data.en')
        guardian_sentence_list = load_txt_file_into_list('../../data/processed_data/guardian_data.txt')
        cnn_sentence_list = load_txt_file_into_list('../../data/processed_data/cnn_data.txt')
        sentence_list = list(set(training_sentence_list + guardian_sentence_list + cnn_sentence_list))
        num_sentences = len(sentence_list)
        print("Number of English sentences: " + str(num_sentences))
        tokenized_sentence_list = [["<SOS>"] + split_text_into_tokens(sentence) + ["<EOS>", "<PAD>"] for sentence in sentence_list]
        num_tokens = sum(len(x) for x in tokenized_sentence_list)
        print("Number of tokens: " + str(num_tokens))
    elif language == "vi":
        training_sentence_list = load_txt_file_into_list('../../data/processed_data/merged_data.vi')
        vnexpress_sentence_list = load_txt_file_into_list('../../data/processed_data/vnexpress_data.txt')
        baomoi_sentence_list = load_txt_file_into_list('../../data/processed_data/baomoi_data.txt')
        sentence_list = list(set(training_sentence_list + vnexpress_sentence_list + baomoi_sentence_list))
        num_sentences = len(sentence_list)
        print("Number of Vietnamese sentences: " + str(num_sentences))
        tokenized_sentence_list = [["<SOS>"] + split_text_into_tokens(sentence) + ["<EOS>", "<PAD>"] for sentence in sentence_list]
        num_tokens = sum(len(x) for x in tokenized_sentence_list)
        print("Number of tokens: " + str(num_tokens))
    return tokenized_sentence_list

def merge_embedding_corpus(language):
    tokenized_sentence_list = load_text_corpus(language)
    word_frequency_counter = Counter(list(itertools.chain.from_iterable(tokenized_sentence_list)))
    tokenized_sentence_list = list(map(lambda sentence: list(map(lambda token: token if word_frequency_counter[token] > 15 else "<UNK>", sentence)),  tokenized_sentence_list))

    if language == "en":
        with open('../../data/processed_data/embedding_corpus.en', 'w', encoding="utf8") as en_embedding_corpus_file:
            for sentence in tokenized_sentence_list:
                en_embedding_corpus_file.write(" ".join(sentence) + "\n")
        en_embedding_corpus_file.close()

    elif language == "vi":
        with open('../../data/processed_data/embedding_corpus.vi', 'w', encoding="utf8") as vi_embedding_corpus_file:
            for sentence in tokenized_sentence_list:
                vi_embedding_corpus_file.write(" ".join(sentence) + "\n")
        vi_embedding_corpus_file.close()

if __name__ == '__main__':
    for lg in ["en", "vi"]:
        merge_embedding_corpus(language=lg)