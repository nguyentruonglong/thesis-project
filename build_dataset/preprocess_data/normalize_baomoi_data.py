import html
import re
from pycorenlp import StanfordCoreNLP
import unicodedata

def load_txt_file_into_list(pathfile):
    txt_file = open(pathfile, 'rt', encoding="utf-8")
    raw_text = txt_file.read().replace("\r", "\n").strip()
    list_of_lines = list(raw_text.split("\n"))
    txt_file.close()
    return list_of_lines

def check_balanced_brackets(array_of_string):
    opening_brackets = {"(","[","{","<","``","`"}
    closing_brackets = {")","]","}",">","''","'"}
    pair_of_brackets = {('(',')'),('[',']'),('{','}'),("``","''"),("`","'"),("<",">")}
    stack = []
    mark_brackets = []
    for i in range(len(array_of_string)):
        if array_of_string[i] in opening_brackets or array_of_string[i] in closing_brackets:
            bracket = array_of_string[i]
            if bracket in opening_brackets:
                stack.append({"bracket": bracket, "position": i})
            elif bracket in closing_brackets:
                if len(stack) == 0:
                    mark_brackets.append({"bracket": bracket, "position": i})
                elif (stack[-1]["bracket"], bracket) not in pair_of_brackets:
                    mark_brackets.append({"bracket": bracket, "position": i})
                elif (stack[-1]["bracket"], bracket) in pair_of_brackets:
                    stack.pop()
    return stack + mark_brackets

def check_person_name(text):
    tokens = list(text.split())
    if "." in tokens:
        tokens.remove(".")
    for token in tokens:
        if token[0] != token[0].upper():
            return False

    return True

def tokenize_sentence(text):
    sentence_list = list()
    nlp = StanfordCoreNLP('http://localhost:8000')

    annotated = nlp.annotate(text, {'annotators': 'ssplit', 'outputFormat': 'json'})
    for sentence_object in annotated['sentences']:
        sentence = list()
        raw_sentence = [obj['word'].replace("--", "–").replace("-LRB-","(").replace("-LCB-","{").replace("-LSB-","[").replace("-RRB-",")").replace("-RCB-","}").replace("-RSB-","]") for obj in sentence_object['tokens']]
        tmp_set = {e['position'] for e in check_balanced_brackets(raw_sentence)}
        if len(raw_sentence) > 0  and len(tmp_set) > 0:
            for ith in range(len(raw_sentence)):
                if ith not in tmp_set and raw_sentence[ith] != "":
                    sentence.append(raw_sentence[ith])
        else:
            sentence = raw_sentence
        
        sentence = ' '.join(sentence).replace("``","\"").replace("''","\"").replace("`","'").strip()
        sentence = re.sub(r'^([\s]*(([0-9]{1,2}|[a-zA-Z])[\s]+)?[\.\-–—)][\s]*)', "", sentence).strip()
        sentence = re.sub(
            r'[^\w\s!"#$%&\'()*+,-.\/:;<=>?@\[\\\]^_`{|}~–—ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹäöüßÄÖÜẞ]+',
            ' ', sentence)
        sentence = re.sub(r'(([\s])[\s]{1,})', ' ', sentence).strip()
        if sentence != "":
            sentence_list.append(sentence)

        if len(sentence_list) > 0:
            if check_person_name(sentence_list[-1]) == True:
                del sentence_list[-1]

    return sentence_list

def normalize_baomoi_data():
    sentence_num = 0
    vi_sentence_set = list()
    list_of_vi_draw_text = load_txt_file_into_list('../../data/tmp/normalized_data_formats/baomoi_content_data.txt')

    with open('../../data/processed_data/baomoi_data.txt', 'w', encoding="utf8") as baomoi_data_file:
        for vi_draw_text in list_of_vi_draw_text:
            try:
                vi_sentence_list = tokenize_sentence(html.unescape(unicodedata.normalize("NFC", vi_draw_text.strip())))
                if len(vi_sentence_list) > 0:
                    for ith_pair in range(len(vi_sentence_list)):
                        vi_sentence = html.unescape(vi_sentence_list[ith_pair])
                        if vi_sentence != "" and "=" not in vi_sentence and "<" not in vi_sentence and ">" not in vi_sentence and "@" not in vi_sentence and "^" not in vi_sentence and "+" not in vi_sentence and "*" not in vi_sentence and "~" not in vi_sentence and "#" not in vi_sentence:
                            sentence_num += 1
                            vi_sentence_set.append(vi_sentence)
                            print("ID: " + str(sentence_num))
                            print(vi_sentence)
                            print("#############################################")

            except Exception:
                print("#############################################")

        vi_sentence_set = list(set(vi_sentence_set))
        for vi_sentence in vi_sentence_set:
             baomoi_data_file.write(vi_sentence + "\r\n")

    baomoi_data_file.close()
    print(len(vi_sentence_set))

normalize_baomoi_data()