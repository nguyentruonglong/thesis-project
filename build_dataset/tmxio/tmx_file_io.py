import os
from lxml import etree
import html

def initialize_tmx_format():
    tmx_tag = etree.Element("tmx", version="1.4")
    header_tag = etree.SubElement(tmx_tag, "header")
    header_tag.attrib['segtype'] = "sentence"
    header_tag.attrib['trglang'] = "VI"
    header_tag.attrib['srclang'] = "EN"
    body_tag = etree.SubElement(tmx_tag, "body")
    return tmx_tag

def append_pairs_of_sentences(tmx_tag, en_sentence, vi_sentence):
    body_tag = tmx_tag.getchildren()[1]
    tu_tag = etree.SubElement(body_tag, "tu")
    tuv_tag = etree.SubElement(tu_tag, 'tuv')
    tuv_tag.attrib['{http://www.w3.org/XML/1998/namespace}lang'] = "EN"
    seg_tag = etree.SubElement(tuv_tag, "seg")
    seg_tag.text = en_sentence

    tuv_tag = etree.SubElement(tu_tag, "tuv")
    tuv_tag.attrib['{http://www.w3.org/XML/1998/namespace}lang'] = "VI"
    seg_tag = etree.SubElement(tuv_tag, "seg")
    seg_tag.text = vi_sentence

def read_data_from_tmx_file(in_filepath):
    ev_sentence_pairs = list()
    parser = etree.XMLParser(recover=True)  # cố gắng phân tích cú pháp của một file XML bị hỏng
    tmx_file = open(in_filepath, 'rt', encoding="utf8")  # open tmx file for reading text data
    tmx_data = html.unescape(tmx_file.read())  # read the entire file into a string variable
    tmx_file.close()    # close the file
    tmx_data = html.unescape(tmx_data)
    tmx_data = html.unescape(tmx_data).replace("&","&amp; ")
    
    root = etree.fromstring(tmx_data, parser=parser)

    for tu_tag in root.iter("tu"):
        ev_sentence_pair = {"en_sent": None, "vi_sent": None}
        for seg_tag in tu_tag.iterdescendants("seg"):
            if seg_tag.getparent().items()[0][1] == "EN":
                ev_sentence_pair["en_sent"] = seg_tag.text
            elif seg_tag.getparent().items()[0][1] == "VI":
                ev_sentence_pair["vi_sent"] = seg_tag.text

        ev_sentence_pairs.append(ev_sentence_pair)
    
    return ev_sentence_pairs

def write_data_into_tmx_file(out_filepath, data):
    tmx_tag = initialize_tmx_format()
    for ev_sentence_pair in data:
        append_pairs_of_sentences(tmx_tag, ev_sentence_pair["en_sent"], ev_sentence_pair["vi_sent"])

    tmx_data = html.unescape(etree.tostring(tmx_tag, encoding='UTF-8', pretty_print=True).decode('utf-8'))  # phương thức unescape giúp chuyển các HTML entities sang các ký tự tương ứng
    tmx_file = open(out_filepath, 'w', encoding="utf8")
    tmx_file.write(tmx_data)
    tmx_file.close()
    print("Completed successfully!")