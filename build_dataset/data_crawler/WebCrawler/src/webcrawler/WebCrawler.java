/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webcrawler;

/**
 *
 * @author NguyenTruongLong
 */
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.google.common.collect.Iterables;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import static java.nio.file.Files.list;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.io.FilenameUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebCrawler {

    static Random rand = new Random();
    //Queue for BFS
    static Queue<String> q = new LinkedList<>();

    //URLs already visited
    static Set<String> marked = new HashSet<>();
    static Set<String> t = new HashSet<>();

    //URL Pattern regex
    //static String regex = "http[s]*://(\\w+\\.)*(\\w+)";

    static String root = "https://www.theguardian.com/";
    static String urlFilepath = System.getProperty("user.dir") + "\\GuardianURLData.txt";
    static String dataFilepath = System.getProperty("user.dir") + "\\GuardianContentData.txt";

    static String[] userAgent = {
        "Mozilla/5.0 (Linux; Android 5.1.1; SM-G928X Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36",
        "Mozilla/5.0 (Linux; Android 6.0.1; E6653 Build/32.2.A.0.253) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36",
        "Mozilla/5.0 (Linux; Android 6.0; HTC One X10 Build/MRA58K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/61.0.3163.98 Mobile Safari/537.36",
        "Mozilla/5.0 (iPhone9,4; U; CPU iPhone OS 10_0_1 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/10.0 Mobile/14A403 Safari/602.1",
        "Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; Lumia 950) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Mobile Safari/537.36 Edge/13.1058",
        "Mozilla/5.0 (Linux; Android 7.0; Pixel C Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/52.0.2743.98 Safari/537.36",
        "Mozilla/5.0 (Linux; Android 6.0.1; SHIELD Tablet K1 Build/MRA58K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Safari/537.36",
        "Mozilla/5.0 (Linux; Android 5.0.2; LG-V410/V41020c Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/34.0.1847.118 Safari/537.36",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36",
        "Mozilla/5.0 (Windows NT 5.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36",
        "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0",
        "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:45.0) Gecko/20100101 Firefox/45.0",
        "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/602.2.14 (KHTML, like Gecko)",};

    public static String getMimeType(String fileUrl)
            throws java.io.IOException, MalformedURLException {
        String type = null;
        URL u = new URL(fileUrl);
        URLConnection uc = null;
        uc = u.openConnection();
        type = uc.getContentType();
        return type;
    }
    

    public static boolean isValidURL(String url) {

        URL u = null;
        try {
            u = new URL(url);
        } catch (MalformedURLException e) {
            return false;
        }
        try {
            u.toURI();
        } catch (URISyntaxException e) {
            return false;
        }
        return true;
    }

    public static void downloadFile(String url) throws MalformedURLException {
        String fileName = String.valueOf(rand.nextInt(899999) + 100000) + "_" + FilenameUtils.getName(new URL(url).getPath());

        try (BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());
                FileOutputStream fileOutputStream = new FileOutputStream("E:\\DataCrawling\\".concat(fileName))) {
            byte dataBuffer[] = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
            }
        } catch (IOException e) {
            // handle exception
        }
    }

    public static void ReadFileLineByLine() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    urlFilepath));
            String line = reader.readLine();
            while (line != null) {
                System.out.println(line);
                q.add(line);
                //t.add(line);
                // read next line
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isMatchesPattern(String url) {
        if (url == null) {
            return false;
        }
        //Pattern regex = Pattern.compile("http[s]*://(www.)?(cep.com.vn/)(.)*");
        //Pattern regex = Pattern.compile("(http[s]*://(www.)?vnexpress.net/[a-zA-Z0-9-/]+.html)");
        Pattern regex = Pattern.compile("(http[s]*://(www.)?theguardian.com/[a-zA-Z0-9-/]+)");
        Matcher matcher = regex.matcher(url);
        if (matcher.find())
        {
            System.out.println(matcher.group(0));
        }
        //url = url.replace(url.replaceAll("(http[s]*://(www.)?(vnexpress.net/)[a-zA-Z0-9-\\/]+.html)", "$2"), "");
        //System.out.println(url);
        return matcher.matches();
    }

    public static void writeFile(String str, String filepath) {

        StringBuilder sb = new StringBuilder();
        String strLine = "";
        try {
            FileWriter fw = new FileWriter(filepath, true);
            //appends the string to the file
            fw.write(str + "\r\n");
            fw.close();
            BufferedReader br = new BufferedReader(new FileReader(filepath));
            //read the file content
            while (strLine != null) {
                sb.append(strLine);
                sb.append(System.lineSeparator());
                strLine = br.readLine();
            }
            br.close();
        } catch (IOException ioe) {
            System.err.println("IOException: " + ioe.getMessage());
        }
    }

    public static void getCEPContent() throws IOException, SocketTimeoutException {
        int articleID = 0, paragraphID = 0;
        ReadFileLineByLine(); // Đọc file chứa các URL vào một danh sách
        while (!q.isEmpty()) {
            try {
                articleID++;
                String url = q.poll();
               //url = "http://webcache.googleusercontent.com/search?q=cache:" + url;
                Document doc = Jsoup.connect(url).header("Accept-Encoding", "gzip, deflate").userAgent(userAgent[rand.nextInt(userAgent.length - 1)]).timeout(100000).ignoreContentType(true).get();
                Element contentsVietnamese = doc.getElementById("vietnamese");
                Element contentsEnglish = doc.getElementById("english");
                if (contentsVietnamese != null) {
                    String processing = "\r\n" + "URL " + articleID + ": " + url + "\r\n###########################################\r\n";
                    System.out.println(processing);
                    String drawContent = "<article data-url=" + url + "data-article-id=" + articleID + ">";
                    List<Element> pTagsVietnamese = contentsVietnamese.select("#vietnamese>p");
                    List<Element> pTagsEnglish = contentsEnglish.select("#english>p");
                    for (int j = 0; j < pTagsVietnamese.size(); j++) {
                        paragraphID++;
                        String drawEnglishText = "\r\n\t<p data-paragraph-id=\"" + paragraphID + "\">\r\n\t\t" + "<span data-language=\"en\">" +  pTagsEnglish.get(j).text().trim() + "</span>";
                        String drawVietnameseText = "\r\n\t\t" + "<span data-language=\"vi\">" + pTagsVietnamese.get(j).text().trim() + "</span>" + "\r\n\t</p>";
                        drawContent = drawContent + drawEnglishText + drawVietnameseText;
                    }
                    
                    drawContent = drawContent + "\r\n</article>";
                    System.out.println(drawContent);
                    writeFile(drawContent, dataFilepath);
                }

            } catch (SocketTimeoutException e) {
                continue;
            } catch (Exception e) {
                continue;
            }
        }
    }

    //BFS Algorithm
    public static void getAllCEPURLs() throws IOException, SocketTimeoutException {

        int urlID = 0;
        q.add(root);
        while (!q.isEmpty()) {
            try {
                String articleURL = q.poll();
                Document doc = Jsoup.connect(articleURL).userAgent(userAgent[rand.nextInt(userAgent.length - 1)]).timeout(100000).ignoreContentType(true).get();
                System.out.println("Processing: " + articleURL);
                System.out.println("###########################");
                Elements atags = doc.getElementsByTag("a");
                
                for (Element atag : atags) {
                    String url = atag.attr("href");
                    if (isValidURL(url) && isMatchesPattern(url)) {
                        if (!marked.contains(url) && !t.contains(url)) {
                            Document article = Jsoup.connect(url).userAgent(userAgent[rand.nextInt(userAgent.length - 1)]).timeout(100000).ignoreContentType(true).get();
                            Element content = article.getElementById("vietnamese");
                            if (content != null) {
                                urlID ++;
                                System.out.println("URL " + urlID + ": " + url);
                                writeFile(url, dataFilepath);
                            }
                            marked.add(url);
                            q.add(url);
                        }
                    }
                }
            } catch (SocketTimeoutException e) {
                continue;
            } catch (Exception e) {
                continue;
            }
        }
    }
    
    public static void getVnexpressURLs() throws IOException, SocketTimeoutException {

        int urlID = 0;
        q.add(root);
        while (!q.isEmpty()) {
            try {
                String articleURL = q.poll();
                Document doc = Jsoup.connect(articleURL).userAgent(userAgent[rand.nextInt(userAgent.length - 1)]).timeout(100000).ignoreContentType(true).get();
                System.out.println("Processing: " + articleURL);
                System.out.println("###########################");
                Elements atags = doc.getElementsByTag("a");
                
                for (Element atag : atags) {
                    String url = atag.attr("href");
                    if (isValidURL(url)) {
                        Pattern regex = Pattern.compile("(http[s]*://(www.)?vnexpress.net/[a-zA-Z0-9-/]+.html)");
                        Matcher matcher = regex.matcher(url);
                        if (matcher.find())
                        {
                            url = matcher.group(0);
                            if (!marked.contains(url)) {
                                writeFile(url, urlFilepath);
                                urlID ++;
                                System.out.println("URL " + urlID + ": " + url);
                                marked.add(url);
                                q.add(url);
                            }
                        }
                    }
                }
            } catch (SocketTimeoutException e) {
                continue;
            } catch (Exception e) {
                continue;
            }
        }
    }

    public static void getVnexpressContent() throws IOException, SocketTimeoutException {
        int articleID = 0;
        ReadFileLineByLine(); // Đọc file chứa các URL vào một danh sách
        while (!q.isEmpty()) {
            try {
                articleID++;
                String url = q.poll();
                System.out.println("URL " + articleID + ": " + url);
               //url = "http://webcache.googleusercontent.com/search?q=cache:" + url;
                Document doc = Jsoup.connect(url).header("Accept-Encoding", "gzip, deflate").userAgent(userAgent[rand.nextInt(userAgent.length - 1)]).timeout(100000).ignoreContentType(true).get();
                Elements contentDetail = doc.getElementsByClass("content_detail");
                if (contentDetail.size() > 0) {
                    String drawContent = contentDetail.get(0).text();
                    System.out.println(drawContent);
                    writeFile(drawContent, dataFilepath);
                }

            } catch (SocketTimeoutException e) {
                continue;
            } catch (Exception e) {
                continue;
            }
        }
    }
    
    public static void getGuardianURLContents() throws IOException, SocketTimeoutException {

        int urlID = 0;
        q.add(root);
        while (!q.isEmpty()) {
            try {
                String articleURL = q.poll();
                Document doc = Jsoup.connect(articleURL).userAgent(userAgent[rand.nextInt(userAgent.length - 1)]).timeout(100000).ignoreContentType(true).get();
                System.out.println("Processing: " + articleURL);
                System.out.println("###########################");
                Elements atags = doc.getElementsByTag("a");
                
                for (Element atag : atags) {
                    String url = atag.attr("href");
                    
                    if (isValidURL(url)) {
                        Pattern regex = Pattern.compile("(http[s]*://(www.)?theguardian.com/[a-zA-Z0-9-/]+)");
                        Matcher matcher = regex.matcher(url);
                        if (matcher.find())
                        {
                            url = matcher.group(0);
                            Document article = Jsoup.connect(url).header("Accept-Encoding", "gzip, deflate").userAgent(userAgent[rand.nextInt(userAgent.length - 1)]).timeout(100000).ignoreContentType(true).get();
                            Elements contentDetail = article.getElementsByClass("content__article-body from-content-api js-article__body");
                            if (contentDetail.size() > 0) {
                                if (!marked.contains(url)) {
                                    String drawContent = contentDetail.get(0).text();
                                    writeFile(url, urlFilepath);
                                    writeFile(drawContent, dataFilepath);
                                    urlID ++;
                                    System.out.println("URL " + urlID + ": " + url);
                                    marked.add(url);
                                    q.add(url);
                                }
                            }
                        }
                    }
                }
            } catch (SocketTimeoutException e) {
                continue;
            } catch (Exception e) {
                continue;
            }
        }
    }
    
    public static String readAllBytes(String filePath)
    {
        String content = "";
 
        try
        {
            content = new String (Files.readAllBytes(Paths.get(filePath)));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
 
        return content;
    }
    
    public static void processCNN() {
        try (Stream<Path> walk = Files.walk(Paths.get("E:\\cnn\\downloads"))) {

		List<String> filepaths = walk.filter(Files::isRegularFile)
				.map(x -> x.toString()).collect(Collectors.toList());

		//result.forEach(System.out::println);
                for (int i = 0; i < filepaths.size(); i++) {
                        System.out.println("Processing: File " + i);
                        System.out.println("###################################");
                        String html = readAllBytes(filepaths.get(i));
                        
                        Document doc = Jsoup.parse(html);
                        Elements contentDetail = doc.getElementsByClass("cnn_storypgraphtxt");
                        if(contentDetail.size() > 0) {
                            for (int j  = 0; j < contentDetail.size(); j++) {
                                String drawContent = contentDetail.get(j).text();
                                writeFile(drawContent, "E:\\CNNData\\" + i + ".txt");
                            }
                        }
                        /*if (contentDetail.hasText()) {
                            String drawContent = contentDetail.text();
                            System.out.println(drawContent);
                            //writeFile(drawContent, dataFilepath);
                        }*/

		}
                /*String html = "<html><head><title>First parse</title></head>"
                + "<body><p>Parsed HTML into a doc.</p></body></html>";
                Document doc = Jsoup.parse(html);*/

	} catch (IOException e) {
		e.printStackTrace();
	}
    }
    public static void main(String[] args) throws IOException, SocketTimeoutException {

        Logger logger = Logger.getLogger("");
        logger.setLevel(Level.OFF);
        //getAllCEPURLs();
        //getVnexpressURLs();
        //getVnexpressContent();
        //System.out.println(urlFilepath);
        //getGuardianURLContents();
        processCNN();
        //System.out.println(isMatchesPattern("https://vnexpress.net/bong-da/ibra-ronaldo-dung-coi-viec-sang-juventus-la-mot-thach-thuc-3881404.html?utm_medium=recommendation&utm_source=the-thao_VnExpress&utm_campaign=VnExpress#c:1_Đen"));
        //getCEPContent();

    }
}
