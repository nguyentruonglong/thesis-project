import tensorflow as tf
from models.seq2seq_model.utils import get_bleu_score
from models.seq2seq_model.inference_fn import inference_fn

def train_and_evaluate(model, model_data, params, model_path):
    """Hyperparameters"""
    num_epochs = params["num_epochs"]
    batch_size = params["batch_size"]
    initial_keep_probability = params["initial_keep_probability"]

    """Model data"""
    split_data_into_batches = model_data["split_data_fn"]
    train_source = model_data["train_source"]
    train_target = model_data["train_target"]
    dev_source = model_data["dev_source"]
    test_source = model_data["test_source"]
    en_vocab_to_id = model_data["index_tables"]["en_vocab_to_id"]
    vi_vocab_to_id = model_data["index_tables"]["vi_vocab_to_id"]
    total_batches = model_data["total_batches"]
    list_of_dev_references = model_data["dev_references"]
    list_of_test_references = model_data["test_references"]

    """Các phần tử của graph"""
    model_graph = model["model_graph"]
    source_input_data = model["source_input_data"]
    target_input_data = model["target_input_data"]
    target_output_data = model["target_output_data"]
    source_input_lengths = model["source_input_lengths"]
    target_output_lengths = model["target_output_lengths"]
    learning_rate = model["learning_rate"]
    train_op = model["train_op"]
    loss = model["loss"]
    keep_prob = model["keep_prob"]
    current_epoch = model["current_epoch"]
    current_learning_rate = model["current_learning_rate"]

    with tf.Session(graph=model_graph) as sess:
        saver = tf.train.Saver()
        state = tf.train.checkpoint_exists(model_path)

        """Khởi tạo các hyperparameters cho mô hình mới hoặc load hyperparameters cho mô hình đang huấn luyện"""
        if state:
            saver.restore(sess, model_path)
            current_learning_rate = model_graph.get_tensor_by_name('current_learning_rate:0')
            current_epoch = model_graph.get_tensor_by_name('current_epoch:0')

            current_epoch_val, current_lr_val = sess.run(
                [current_epoch, current_learning_rate])
        else:
            sess.run(tf.global_variables_initializer())
            current_epoch_val, current_lr_val = sess.run(
                [current_epoch, current_learning_rate])

        training_log_filename = model_path.replace("model", "") + "training_log.txt"
        translation_file_of_test_data = model_path.replace("model", "") + 'test_data_translations.txt'
        try:
            tmp_log_file = open(training_log_filename, 'rt', encoding="utf-8")
            content_log_file = tmp_log_file.read().strip()
            print(content_log_file)
            tmp_log_file.close()
        except FileNotFoundError:
            print("")

        """Tensorboard"""
        tensorboard_dir = model_path.replace("model", "") + "tensorboard"
        train_writer = tf.summary.FileWriter(tensorboard_dir, model_graph)
        global_step = 0
        dev_bleu_placeholder = tf.placeholder(tf.float64, [])
        dev_bleu_summary_tensor = tf.summary.scalar('dev_bleu', dev_bleu_placeholder)

        for epoch_i in range(current_epoch_val, num_epochs + 1):
            tmp_log = current_result = str()

            # Train
            for batch_i, (
            train_source_input_batch, train_source_input_lengths, train_target_input_batch, train_target_output_batch,
            train_target_output_lengths) in enumerate(split_data_into_batches(train_source, train_target, batch_size, en_vocab_to_id, vi_vocab_to_id), 1):			
                _, loss_val = sess.run(
                    [train_op, loss],
                    {source_input_data: train_source_input_batch,
                     target_input_data: train_target_input_batch,
                     target_output_data: train_target_output_batch,
                     source_input_lengths: train_source_input_lengths,
                     target_output_lengths: train_target_output_lengths,
                     learning_rate: current_lr_val,
                     keep_prob: initial_keep_probability
                     })

                if batch_i % 100 == 0 or batch_i == total_batches:
                    current_result = "Epoch: " + str(epoch_i) + "/" + str(num_epochs) + " - Batch: " + str(
                        batch_i) + "/" + str(total_batches) + " - Learning rate: " + str(
                        current_lr_val) + " - Loss: " + str(loss_val)

                    # Evaluate
                    if batch_i % 1000 == 0 or batch_i == total_batches:
                        processed_dev_translations = inference_fn(sess, dev_source, model, model_data, params)
                        dev_bleu_score = get_bleu_score(list_of_dev_references, processed_dev_translations)

                        """Lưu Dev BLEU trên Tensorboard"""
                        dev_bleu_summary = sess.run(dev_bleu_summary_tensor, {dev_bleu_placeholder: dev_bleu_score})
                        train_writer.add_summary(dev_bleu_summary, global_step)

                        current_result += " - Dev BLEU: " + str(dev_bleu_score)
												
                    # Test
                    if epoch_i == num_epochs and batch_i == total_batches:

                        processed_test_translations = inference_fn(sess, test_source, model, model_data, params)
                        test_bleu_score = get_bleu_score(list_of_test_references, processed_test_translations)

                        current_result += " - Test BLEU: " + str(test_bleu_score)

                        with open(translation_file_of_test_data, 'a', encoding="utf-8") as translations_file:
                            for sentence in processed_test_translations:
                                translations_file.write(" ".join(sentence) + "\r\n")

                    print(current_result)
                    tmp_log += current_result + "\r\n"

                global_step += 1

            with open(training_log_filename, 'a', encoding="utf-8") as log_file:
                log_file.write(tmp_log)

            if epoch_i >= 7:
                current_lr_val /= 2
            # Save Model
            sess.run([tf.assign(current_epoch, epoch_i + 1), tf.assign(current_learning_rate, current_lr_val)])
            saver.save(sess, model_path)
            print('Model has been successfully saved!')




