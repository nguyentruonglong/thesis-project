import math
import collections
import json
import os
import numpy as np
from gensim.models import KeyedVectors
import matplotlib.pyplot as plt

def print_params(params):
    decoder_mode = params["decoder_mode"].replace("_", " ")
    embedding_method = params["embedding_params"]["embedding_method"]
    if params["embedding_params"]["embedding_method"] == "gaussian_random":
        standard_deviation = params["embedding_params"]["standard_deviation"] 	
        print('Decoder Mode: {} - Embedding Method: {} - Standard Deviation: {}'.format(decoder_mode, embedding_method, standard_deviation))
    else:
        window_size = params["embedding_params"]["window_size"]	
        print('Decoder Mode: {} - Embedding Method: {} - Window Size: {}'.format(decoder_mode, embedding_method, window_size))	

def get_ngrams(segment, max_order):
    ngram_counts = collections.Counter()
    for order in range(1, max_order + 1):
        for i in range(0, len(segment) - order + 1):
            ngram = tuple(segment[i:i + order])
            ngram_counts[ngram] += 1
    return ngram_counts


def get_bleu_score(reference_corpus, translation_corpus, max_order=4):
    matches_by_order = [0] * max_order
    possible_matches_by_order = [0] * max_order
    reference_length = 0
    translation_length = 0
    for (references, translation) in zip(reference_corpus,
                                         translation_corpus):
        reference_length += min(len(r) for r in references)
        translation_length += len(translation)

        merged_ref_ngram_counts = collections.Counter()
        for reference in references:
            merged_ref_ngram_counts |= get_ngrams(reference, max_order)
        translation_ngram_counts = get_ngrams(translation, max_order)
        overlap = translation_ngram_counts & merged_ref_ngram_counts
        for ngram in overlap:
            matches_by_order[len(ngram) - 1] += overlap[ngram]
        for order in range(1, max_order + 1):
            possible_matches = len(translation) - order + 1
            if possible_matches > 0:
                possible_matches_by_order[order - 1] += possible_matches

    precisions = [0] * max_order
    for i in range(0, max_order):
        if possible_matches_by_order[i] > 0:
            precisions[i] = (float(matches_by_order[i]) / possible_matches_by_order[i])
        else:
            precisions[i] = 0.0

    if min(precisions) > 0:
        p_log_sum = sum((1. / max_order) * math.log(p) for p in precisions)
        geo_mean = math.exp(p_log_sum)
    else:
        geo_mean = 0

    ratio = float(translation_length) / reference_length

    if ratio > 1.0:
        bp = 1.
    elif ratio == 0.:
        bp = math.exp(1)
    else:
        bp = math.exp(1 - 1. / ratio)

    bleu = geo_mean * bp
    return bleu


def load_json(pathfile):
    with open(pathfile, 'r') as json_file:
        json_object = json.load(json_file)
    return json_object


def generate_random_embeddings(sd_val, loc_val=0.0):

    """Get vocab and vector size from Word2vec embeddings"""

    en_cbow_embedding = KeyedVectors.load_word2vec_format(
        "../../experiments/word_embeddings/word2vec/cbow/window_size_2/en_embedding.txt",
        binary=False)
    vi_cbow_embedding = KeyedVectors.load_word2vec_format(
        "../../experiments/word_embeddings/word2vec/cbow/window_size_2/vi_embedding.txt",
        binary=False)

    en_vector_size = en_cbow_embedding.vector_size
    vi_vector_size = vi_cbow_embedding.vector_size

    en_vocab = [word for word in list(en_cbow_embedding.wv.vocab) if word not in ["<PAD>", "<SOS>", "<EOS>", "<UNK>"]]
    en_vocab.sort()
    en_vocab = ["<PAD>", "<SOS>", "<EOS>", "<UNK>"] + en_vocab

    vi_vocab = [word for word in list(vi_cbow_embedding.wv.vocab) if word not in ["<PAD>", "<SOS>", "<EOS>", "<UNK>"]]
    vi_vocab.sort()
    vi_vocab = ["<PAD>", "<SOS>", "<EOS>", "<UNK>"] + vi_vocab

    folder_path = "standard_deviation_" + str(sd_val)	
	
    """Generate random embeddings for English and Vietnamese"""
    en_random_vectors = np.random.normal(loc=loc_val, scale=sd_val, size=[len(en_vocab), en_vector_size])

    """Save English random embeddings"""
    with open('../../experiments/word_embeddings/gaussian_random/' + folder_path + '/en_embedding.txt', 'w',
              encoding="utf-8") as en_embedding_file:
        en_embedding_file.write(str(len(en_vocab)) + " " + str(en_vector_size))
        for i in range(len(en_vocab)):
            en_word_vector = np.concatenate(([en_vocab[i]], en_random_vectors[i]))
            en_embedding_file.write("\n")
            np.savetxt(en_embedding_file, en_word_vector, newline=" ", fmt="%s")
        en_embedding_file.close()
    print("Random English word embeddings generated!")

    vi_random_vectors = np.random.normal(loc=0.0, scale=sd_val, size=[len(vi_vocab), vi_vector_size])
    """Save Vietnamese random embeddings"""
    with open('../../experiments/word_embeddings/gaussian_random/' + folder_path + '/vi_embedding.txt', 'w',
              encoding="utf-8") as vi_embedding_file:
        vi_embedding_file.write(str(len(vi_vocab)) + " " + str(vi_vector_size))
        for i in range(len(vi_vocab)):
            vi_word_vector = np.concatenate(([vi_vocab[i]], vi_random_vectors[i]))
            vi_embedding_file.write("\n")
            np.savetxt(vi_embedding_file, vi_word_vector, newline=" ", fmt="%s")
        vi_embedding_file.close()
    print("Random Vietnamese word embeddings generated!")


#generate_random_embeddings(0.5)
#generate_random_embeddings(0.05)