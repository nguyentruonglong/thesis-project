import math
import numpy as np
from gensim.models import KeyedVectors


def split_text_into_tokens(text):
    return list(text.split())

def load_word_embedding(language, params):
    '''Cần thêm trường hợp cho GloVe'''
    embedding_method = params["embedding_params"]["embedding_method"]
    window_size = params["embedding_params"]["window_size"]

    if embedding_method == "cbow" or embedding_method == "skip_gram":
        txt_model_file_name = "experiments/word_embeddings/word2vec/" + embedding_method + "/" + "window_size_" + str(
            window_size) + "/" + language.lower() + "_embedding.txt"
    elif embedding_method == "glove":
        txt_model_file_name = "experiments/word_embeddings/glove/" + "window_size_" + str(
            window_size) + "/" + language.lower() + "_embedding.txt"			
    elif embedding_method == "gaussian_random":
        standard_deviation = "standard_deviation_" + str(params["embedding_params"]["standard_deviation"]) + "/"	
        txt_model_file_name = "experiments/word_embeddings/gaussian_random/" + standard_deviation + language.lower() + "_embedding.txt"

    model = KeyedVectors.load_word2vec_format(txt_model_file_name, binary=False)
    return model


def get_vocabs_and_embeddings(params):
    en_embedding = load_word_embedding("en", params)
    vi_embedding = load_word_embedding("vi", params)

    en_vocab = [word for word in list(en_embedding.wv.vocab) if word not in ["<PAD>", "<SOS>", "<EOS>", "<UNK>"]]
    en_vocab.sort()
    en_vocab = ["<PAD>", "<SOS>", "<EOS>", "<UNK>"] + en_vocab

    vi_vocab = [word for word in list(vi_embedding.wv.vocab) if word not in ["<PAD>", "<SOS>", "<EOS>", "<UNK>"]]
    vi_vocab.sort()
    vi_vocab = ["<PAD>", "<SOS>", "<EOS>", "<UNK>"] + vi_vocab

    en_embedding_table = [list(en_embedding[word]) for word in en_vocab]
    vi_embedding_table = [list(vi_embedding[word]) for word in vi_vocab]

    return {
        "en_vocab": en_vocab,
        "vi_vocab": vi_vocab,
        "en_embedding_table": en_embedding_table,
        "vi_embedding_table": vi_embedding_table
    }


def load_txt_file_into_list(pathfile):
    txt_file = open(pathfile, 'rt', encoding="utf-8")
    raw_text = txt_file.read().replace("\r", "\n").strip()
    list_of_lines = list(raw_text.split("\n\n"))
    txt_file.close()
    return list_of_lines


def load_training_data(training_source_pathfile, training_target_pathfile):
    en_train_data = load_txt_file_into_list(training_source_pathfile)
    vi_train_data = load_txt_file_into_list(training_target_pathfile)
    return en_train_data, vi_train_data


def load_dev_data(dev_source_pathfile, dev_target_pathfile):
    en_dev_data = load_txt_file_into_list(dev_source_pathfile)
    vi_dev_data = load_txt_file_into_list(dev_target_pathfile)
    return en_dev_data, vi_dev_data


def load_test_data(test_source_pathfile, test_target_pathfile):
    en_test_data = load_txt_file_into_list(test_source_pathfile)
    vi_test_data = load_txt_file_into_list(test_target_pathfile)
    return en_test_data, vi_test_data


def get_index_tables(vi_vocab, en_vocab):
    '''
    Tạo các table {index, word} và {word, index} trong ngôn ngữ tiếng Việt
    Table này sử dụng cho mục đích tra từ trong quá trình tạo ra bản dịch
    :return: 2 bảng tra với kiểu dữ liệu Dictionary
    '''
    id_to_vi_vocab = {index: word for index, word in enumerate(vi_vocab)}
    vi_vocab_to_id = {value: key for key, value in id_to_vi_vocab.items()}
    id_to_en_vocab = {index: word for index, word in enumerate(en_vocab)}
    en_vocab_to_id = {value: key for key, value in id_to_en_vocab.items()}

    return {
        "id_to_vi_vocab": id_to_vi_vocab,
        "vi_vocab_to_id": vi_vocab_to_id,
        "id_to_en_vocab": id_to_en_vocab,
        "en_vocab_to_id": en_vocab_to_id
    }


def map_sentence_batch_to_id(sentence_batch, vocab_to_id):
    tokenized_sentence_batch = [split_text_into_tokens(sentence) for sentence in sentence_batch]
    sentence_batch_in_id = [[vocab_to_id[word] if word in vocab_to_id else vocab_to_id["<UNK>"] for word in sentence]
                            for sentence in tokenized_sentence_batch]

    return sentence_batch_in_id


def process_source_input_data(sentence_batch, vocab_to_id):
    # Chuyển câu sang id
    pad_id = vocab_to_id["<PAD>"]
    sentence_batch_in_id = map_sentence_batch_to_id(sentence_batch, vocab_to_id)
    max_sentence_length = max([len(sentence_in_id) for sentence_in_id in sentence_batch_in_id])
    padded_sentence_batch_in_id = [sentence_in_id + [
        pad_id] * (max_sentence_length - len(sentence_in_id)) for
                                   sentence_in_id in sentence_batch_in_id]
    source_input_lengths = [len(sentence_in_id) for sentence_in_id in padded_sentence_batch_in_id]

    return np.array(padded_sentence_batch_in_id), np.array(source_input_lengths)


def process_target_input_data(sentence_batch, vocab_to_id):
    sos_id = vocab_to_id["<SOS>"]
    pad_id = vocab_to_id["<PAD>"]
    sentence_batch_in_id = map_sentence_batch_to_id(sentence_batch, vocab_to_id)
    max_sentence_length = max([len(sentence_in_id) for sentence_in_id in sentence_batch_in_id])
    padded_sentence_batch_in_id = [[sos_id] + sentence_in_id + [pad_id] * (max_sentence_length - len(sentence_in_id))
                                   for
                                   sentence_in_id in sentence_batch_in_id]
    return np.array(padded_sentence_batch_in_id)


def process_target_output_data(sentence_batch, vocab_to_id):
    eos_id = vocab_to_id["<EOS>"]
    pad_id = vocab_to_id["<PAD>"]
    sentence_batch_in_id = map_sentence_batch_to_id(sentence_batch, vocab_to_id)
    padded_sentence_batch_in_id = [sentence_in_id + [eos_id] for sentence_in_id in sentence_batch_in_id]
    max_sentence_length = max([len(sentence_in_id) for sentence_in_id in padded_sentence_batch_in_id])
    padded_sentence_batch_in_id = [sentence_in_id + [pad_id] * (max_sentence_length - len(sentence_in_id)) for
                                   sentence_in_id in padded_sentence_batch_in_id]
    target_output_lengths = [len(sentence_in_id) for sentence_in_id in padded_sentence_batch_in_id]

    return np.array(padded_sentence_batch_in_id), np.array(target_output_lengths)


def split_data_into_batches(source_data, target_data, batch_size, en_vocab_to_id, vi_vocab_to_id=None):
    '''Xử lý tách dữ liệu đầu vào thành các mini-batch'''

    batch_length = len(source_data)
    num_batches = math.ceil(batch_length / batch_size)
    if target_data != None:
        for batch_i in range(num_batches):
            start_i = batch_i * batch_size
            end_i = start_i + batch_size if (start_i + batch_size) < batch_length else batch_length
            en_sentence_batch = source_data[start_i:end_i]
            vi_sentence_batch = target_data[start_i:end_i]

            # Trả về 1 batch các câu source dưới dạng id và độ dài mỗi câu sau khi pad
            source_input_data, source_input_lengths = process_source_input_data(en_sentence_batch, en_vocab_to_id)
            # Trả về 1 batch các câu target dưới dạng id
            target_input_data = process_target_input_data(vi_sentence_batch, vi_vocab_to_id)
            # Trả về 1 batch các câu target dưới dạng label và độ dài mỗi câu sau khi pad
            target_output_data, target_output_lengths = process_target_output_data(vi_sentence_batch, vi_vocab_to_id)
            yield source_input_data, source_input_lengths, target_input_data, target_output_data, target_output_lengths
    else:
        for batch_i in range(num_batches):
            start_i = batch_i * batch_size
            end_i = start_i + batch_size if (start_i + batch_size) < batch_length else batch_length
            en_sentence_batch = source_data[start_i:end_i]

            # Trả về 1 batch các câu source dưới dạng id và độ dài mỗi câu sau khi pad
            source_input_data, source_input_lengths = process_source_input_data(en_sentence_batch, en_vocab_to_id)
            yield source_input_data, source_input_lengths


def get_model_data(params, mode):

    vocabs_and_embeddings = get_vocabs_and_embeddings(params)
    index_tables = get_index_tables(vocabs_and_embeddings["vi_vocab"], vocabs_and_embeddings["en_vocab"])

    if mode == "predict":
        return {
            "split_data_fn": split_data_into_batches,
            "vocabs_and_embeddings": vocabs_and_embeddings,
            "index_tables": index_tables
        }
    else:
        batch_size = params["batch_size"]
        train_source_path = "data/processed_data/merged_data.en"
        train_target_path = "data/processed_data/merged_data.vi"
        dev_source_path = "data/processed_data/iwslt12_data.en"
        dev_target_path = "data/processed_data/iwslt12_data.vi"
        test_source_path = "data/processed_data/iwslt13_data.en"
        test_target_path = "data/processed_data/iwslt13_data.vi"

        train_source, train_target = load_training_data(train_source_path, train_target_path)
        dev_source, dev_target = load_dev_data(dev_source_path, dev_target_path)
        test_source, test_target = load_test_data(test_source_path, test_target_path)
        dev_references = list(map(lambda x: [split_text_into_tokens(x)], dev_target))
        test_references = list(map(lambda x: [split_text_into_tokens(x)], test_target))
        total_batches = math.ceil(len(train_source) / batch_size)

        return {
            "train_source" : train_source,
            "train_target" : train_target,
            "dev_source" : dev_source,
            "dev_target" : dev_target,
            "test_source" : test_source,
            "test_target" : test_target,
            "split_data_fn" : split_data_into_batches,
            "dev_references" : dev_references,
            "test_references" : test_references,
            "vocabs_and_embeddings" : vocabs_and_embeddings,
            "index_tables" : index_tables,
            "total_batches" : total_batches
        }