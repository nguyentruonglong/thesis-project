def map_batch_id_to_sentence(sentence_batch_in_id, id_to_vocab):
    sentence_batch = [[id_to_vocab[id] for id in sentence] for sentence in sentence_batch_in_id]
    return sentence_batch

def postprocess_data(sentence_batch_in_id, id_to_vocab, vocab_to_id):
    eos_id = vocab_to_id["<EOS>"]
    pad_id = vocab_to_id["<PAD>"]
    sentence_batch_in_id = [filter(lambda x: x != eos_id and x != pad_id, sentence_in_id) for sentence_in_id in
                            sentence_batch_in_id]
    sentence_batch = map_batch_id_to_sentence(sentence_batch_in_id, id_to_vocab)
    return sentence_batch

def get_translations(sess, list_input_sentences, model, model_data, params):
    """Các phần tử của graph"""
    inference_output = model["inference_output"]
    source_input_data = model["source_input_data"]
    source_input_lengths = model["source_input_lengths"]
    #target_output_lengths = model["target_output_lengths"]

    keep_prob = model["keep_prob"]
    id_to_vi_vocab = model_data["index_tables"]["id_to_vi_vocab"]
    en_vocab_to_id = model_data["index_tables"]["en_vocab_to_id"]
    vi_vocab_to_id = model_data["index_tables"]["vi_vocab_to_id"]

    batch_size = params["batch_size"]
    split_data_into_batches = model_data["split_data_fn"]


    translations_in_id = list()
    for batch_i, (
            source_input_batch, source_input_batch_lengths) in enumerate(
        split_data_into_batches(list_input_sentences, None, batch_size, en_vocab_to_id)):
        batch_translations = sess.run(
            inference_output,
            {source_input_data: source_input_batch,
             source_input_lengths: source_input_batch_lengths,
             #target_output_lengths: source_input_batch_lengths,
             keep_prob: 1.0
             })

        translations_in_id += batch_translations.tolist()
		
    processed_translations = postprocess_data(translations_in_id, id_to_vi_vocab, vi_vocab_to_id)
    return processed_translations

def inference_fn(sess, en_sentences, model, model_data, params):
    translations = get_translations(sess, en_sentences, model, model_data, params)
    return translations