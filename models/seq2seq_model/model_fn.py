import time
import datetime
import pytz

import tensorflow as tf

"""Khởi tạo cho mô hình"""


def initialize_model_inputs():
    with tf.name_scope("model_inputs"):
        # Các câu nguồn tiếng Anh trong 1 batch dưới dạng vector
        source_input_data = tf.placeholder(tf.int32, [None, None], name='source_input_data')
        # Chứa độ dài các câu trong batch tiếng Anh sau khi đã pad ký tự <pad>
        source_input_lengths = tf.placeholder(tf.int32, [None], name='source_input_lengths')
        # Các câu đích tiếng Việt trong 1 batch dưới dạng vector
        target_input_data = tf.placeholder(tf.int32, [None, None], name='target_input_data')
        # Các nhãn tương ứng với các từ câu tiếng Việt trong 1 batch
        target_output_data = tf.placeholder(tf.int32, [None, None], name='target_output_data')
        # Chứa độ dài các câu trong batch tiếng Việt sau khi đã pad ký tự <pad>
        target_output_lengths = tf.placeholder(tf.int32, [None], name='target_output_lengths')

    return source_input_data, source_input_lengths, target_input_data, target_output_data, target_output_lengths


def initialize_hyperparameters():
    with tf.name_scope("hyperparameters"):
        # Khởi tạo learning rate
        learning_rate = tf.placeholder(tf.float64, name='learning_rate')
        # Khởi tạo keep prob
        keep_prob = tf.placeholder(tf.float32, name='keep_prob')

        return learning_rate, keep_prob


def initialize_state_parameters(params):
    """Hyperparameters"""
    initial_learning_rate = params["initial_learning_rate"]

    current_epoch = tf.Variable(1, dtype=tf.int32, name="current_epoch", trainable=False)
    current_learning_rate = tf.Variable(initial_learning_rate, dtype=tf.float64, name="current_learning_rate",
                                        trainable=False)

    return current_epoch, current_learning_rate


"""Encoder"""


def encoder_layer(encoder_input, source_sequence_length, keep_prob, model_data, params):
    """Hyperparameters"""
    num_units = params["num_units"]
    num_layers = params["num_layers"]
    unit_type = params["unit_type"]  # "lstm" or "gru"

    en_embedding_table = model_data["vocabs_and_embeddings"]["en_embedding_table"]

    with tf.name_scope("encoder"):
        num_sentences = tf.size(source_sequence_length)
        embedding_encoder = tf.constant(en_embedding_table, name="embedding_encoder")
        encoder_embed_input = tf.nn.embedding_lookup(embedding_encoder, encoder_input)

        encoder_cells = tf.contrib.rnn.MultiRNNCell(
            [tf.contrib.rnn.DropoutWrapper(tf.contrib.rnn.LSTMCell(num_units),
                                               keep_prob) for _ in range(num_layers)])

        initial_state = encoder_cells.zero_state(num_sentences, dtype=tf.float32)

        encoder_outputs, encoder_state = tf.nn.dynamic_rnn(encoder_cells,
                                                           encoder_embed_input,
                                                           initial_state=initial_state,
                                                           dtype=tf.float32)
        return encoder_outputs, encoder_state


"""Decoder"""


def attention_greedy_decoder(encoder_outputs, encoder_state, source_sequence_length, decoder_input,
                             target_sequence_length, keep_prob, model_data, params):
    """Hyperparameters"""
    num_units = params["num_units"]
    num_layers = params["num_layers"]
    unit_type = params["unit_type"]  # "lstm" or "gru"

    vi_embedding_table = model_data["vocabs_and_embeddings"]["vi_embedding_table"]
    vi_vocab = model_data["vocabs_and_embeddings"]["vi_vocab"]
    vi_vocab_to_id = model_data["index_tables"]["vi_vocab_to_id"]

    vi_vocab_size = len(vi_vocab)
    vi_sos_id = vi_vocab_to_id["<SOS>"]
    vi_eos_id = vi_vocab_to_id["<EOS>"]
    num_sentences = tf.size(source_sequence_length)
    maximum_iterations = tf.reduce_max(source_sequence_length) * 2
    embedding_decoder = tf.constant(vi_embedding_table, name="embedding_decoder")
    decoder_embed_input = tf.nn.embedding_lookup(embedding_decoder, decoder_input)

    decoder_cells = tf.contrib.rnn.MultiRNNCell(
        [tf.contrib.rnn.DropoutWrapper(tf.contrib.rnn.LSTMCell(num_units),
                                           keep_prob) for _ in range(num_layers)])

    projection_layer = tf.layers.Dense(vi_vocab_size, use_bias=False)

    # Tạo cơ chế Luong Attention
    attention_mechanism = tf.contrib.seq2seq.LuongAttention(
        num_units, encoder_outputs, memory_sequence_length=source_sequence_length)

    # Attention wrapper, alignment_history là True khi inference không sử dụng Beam Search
    attention_decoder_cells = tf.contrib.seq2seq.AttentionWrapper(
        decoder_cells,
        attention_mechanism,
        attention_layer_size=num_units,
        alignment_history=True,
        output_attention=True,
        name="attention")

    # Trạng thái khởi tạo của decoder, sử dụng encoder_state
    decoder_initial_state = attention_decoder_cells.zero_state(batch_size=num_sentences, dtype=tf.float32).clone(
        cell_state=encoder_state)

    with tf.variable_scope("training_decoder"):
        # Helper

        helper = tf.contrib.seq2seq.TrainingHelper(
            decoder_embed_input, sequence_length=target_sequence_length)

        decoder = tf.contrib.seq2seq.BasicDecoder(
            attention_decoder_cells, helper,
            initial_state=decoder_initial_state,
            output_layer=projection_layer)
        # Dynamic decoding
        outputs, _, _ = tf.contrib.seq2seq.dynamic_decode(decoder,
                                                          impute_finished=True,
                                                          maximum_iterations=None)
        training_logits = outputs

    with tf.variable_scope("inference_decoder", reuse=True):
        start_tokens = tf.map_fn(lambda x: vi_sos_id, source_sequence_length)

        # Helper
        helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(embedding_decoder,
                                                          start_tokens,
                                                          vi_eos_id)

        # Decoder
        decoder = tf.contrib.seq2seq.BasicDecoder(
            attention_decoder_cells, helper,
            initial_state=decoder_initial_state,
            output_layer=projection_layer)

        prediction_outputs, _, _ = tf.contrib.seq2seq.dynamic_decode(decoder,
                                                                     impute_finished=True,
                                                                     maximum_iterations=maximum_iterations)

    return training_logits, prediction_outputs


def attention_beam_decoder(encoder_outputs, encoder_state, source_sequence_length, decoder_input,
                           target_sequence_length, keep_prob, model_data, params):
    """Hyperparameters"""
    num_units = params["num_units"]
    num_layers = params["num_layers"]
    beam_width = params["beam_width"]

    vi_embedding_table = model_data["vocabs_and_embeddings"]["vi_embedding_table"]
    vi_vocab = model_data["vocabs_and_embeddings"]["vi_vocab"]
    vi_vocab_to_id = model_data["index_tables"]["vi_vocab_to_id"]

    vi_vocab_size = len(vi_vocab)
    vi_sos_id = vi_vocab_to_id["<SOS>"]
    vi_eos_id = vi_vocab_to_id["<EOS>"]
    num_sentences = tf.size(source_sequence_length)
    maximum_iterations = tf.reduce_max(source_sequence_length) * 2
    embedding_decoder = tf.constant(vi_embedding_table, name="embedding_decoder")
    decoder_embed_input = tf.nn.embedding_lookup(embedding_decoder, decoder_input)

    decoder_cells = tf.contrib.rnn.MultiRNNCell(
        [tf.contrib.rnn.DropoutWrapper(tf.contrib.rnn.LSTMCell(num_units),
                                               keep_prob) for _ in range(num_layers)])
    projection_layer = tf.layers.Dense(vi_vocab_size, use_bias=False)

    with tf.variable_scope("training_decoder"):
        # Tạo cơ chế Luong Attention
        attention_mechanism = tf.contrib.seq2seq.LuongAttention(
            num_units, encoder_outputs, memory_sequence_length=source_sequence_length)
        # Attention wrapper
        attention_decoder_cells = tf.contrib.seq2seq.AttentionWrapper(
            decoder_cells,
            attention_mechanism,
            attention_layer_size=num_units,
            alignment_history=False,
            output_attention=True)
        # Khởi tạo hidden state
        decoder_initial_state = attention_decoder_cells.zero_state(dtype=tf.float32, batch_size=num_sentences).clone(
            cell_state=encoder_state)

        # Helper
        helper = tf.contrib.seq2seq.TrainingHelper(
            decoder_embed_input, sequence_length=target_sequence_length)

        decoder = tf.contrib.seq2seq.BasicDecoder(
            attention_decoder_cells, helper,
            initial_state=decoder_initial_state,
            output_layer=projection_layer)

        # Dynamic decoding
        outputs, _, _ = tf.contrib.seq2seq.dynamic_decode(decoder,
                                                          impute_finished=True,
                                                          maximum_iterations=None)
        training_logits = outputs

    with tf.variable_scope("training_decoder", reuse=True):
        start_tokens = tf.map_fn(lambda x: vi_sos_id, source_sequence_length)
        # Tăng kích thước dữ liệu lên beam_width lần
        tiled_encoder_outputs = tf.contrib.seq2seq.tile_batch(encoder_outputs, multiplier=beam_width)
        tiled_encoder_final_state = tf.contrib.seq2seq.tile_batch(encoder_state, multiplier=beam_width)
        tiled_source_sequence_length = tf.contrib.seq2seq.tile_batch(source_sequence_length, multiplier=beam_width)
        # Tạo cơ chế Luong Attention
        beam_attention_mechanism = tf.contrib.seq2seq.LuongAttention(
            num_units, tiled_encoder_outputs, memory_sequence_length=tiled_source_sequence_length)
        # Attention wrapper
        beam_attention_decoder_cells = tf.contrib.seq2seq.AttentionWrapper(
            decoder_cells,
            beam_attention_mechanism,
            attention_layer_size=num_units,
            alignment_history=False,
            output_attention=True)
        # Khởi tạo hidden state
        beam_decoder_initial_state = beam_attention_decoder_cells.zero_state(dtype=tf.float32,
                                                                             batch_size=num_sentences * beam_width).clone(
            cell_state=tiled_encoder_final_state)

        beam_decoder = tf.contrib.seq2seq.BeamSearchDecoder(
            cell=beam_attention_decoder_cells,
            embedding=embedding_decoder,
            start_tokens=start_tokens,
            end_token=vi_eos_id,
            initial_state=beam_decoder_initial_state,
            beam_width=beam_width,
            output_layer=projection_layer,
            length_penalty_weight=0.0)

        prediction_outputs, _, _ = tf.contrib.seq2seq.dynamic_decode(beam_decoder,
                                                                     impute_finished=False,
                                                                     maximum_iterations=maximum_iterations)
        translations = prediction_outputs.predicted_ids  # [batch_size, num_steps, beam_width]
        translations = tf.transpose(translations, perm=[0, 2, 1])  # [batch_size, beam_width, num_steps]
        best_translation = []
        best_translation = tf.map_fn(lambda x: tf.concat([best_translation, x[0]], 0), translations)

    return training_logits, best_translation


def decoder_layer(encoder_outputs, encoder_state, source_sequence_length, decoder_input, target_sequence_length,
                  keep_prob, model_data, params):
    """Hyperparameters"""
    decoder_mode = params["decoder_mode"]  # "greedy_search" or "beam_search"

    # Giải thuật tìm kiếm cho Attention Decoder 
    if decoder_mode == "greedy_search":
        training_logits, prediction_outputs = attention_greedy_decoder(encoder_outputs, encoder_state,
                                                                       source_sequence_length, decoder_input,
                                                                       target_sequence_length, keep_prob, model_data,
                                                                       params)
        return training_logits.rnn_output, prediction_outputs.sample_id
    elif decoder_mode == "beam_search":
        training_logits, prediction_outputs = attention_beam_decoder(encoder_outputs, encoder_state,
                                                                     source_sequence_length, decoder_input,
                                                                     target_sequence_length, keep_prob, model_data,
                                                                     params)
        return training_logits.rnn_output, prediction_outputs


def build_seq2seq_model(encoder_input, source_sequence_length, decoder_input, target_sequence_length, keep_prob,
                        model_data, params):
    '''Xây dựng mô hình Seq2Seq'''
    encoder_outputs, encoder_state = encoder_layer(encoder_input, source_sequence_length, keep_prob, model_data, params)
    logits, infer_logits = decoder_layer(encoder_outputs, encoder_state, source_sequence_length, decoder_input,
                                         target_sequence_length, keep_prob, model_data, params)
    return tf.identity(logits, name="training_output"), tf.identity(infer_logits, name="inference_output")


def get_model(model_data, params, mode, model_path):

    if mode == "train_and_eval":
        model_graph = tf.Graph()
        with model_graph.as_default():
            source_input_data, source_input_lengths, target_input_data, target_output_data, target_output_lengths = initialize_model_inputs()
            current_epoch, current_learning_rate = initialize_state_parameters(params)
            learning_rate, keep_prob = initialize_hyperparameters()

            training_output, inference_output = build_seq2seq_model(source_input_data, source_input_lengths,
                                                                    target_input_data, target_output_lengths, keep_prob,
                                                                    model_data, params)

            # Masks
            masks = tf.ones_like(target_output_data, dtype=tf.float32)
            with tf.name_scope("optimizer"):
                # Tính mất mát bằng hàm cross-entropy
                loss = tf.contrib.seq2seq.sequence_loss(logits=training_output, targets=target_output_data, weights=masks)
                opt = tf.train.AdamOptimizer(learning_rate=learning_rate)

                grads_and_vars = opt.compute_gradients(loss)
                capped_grads_and_vars = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in grads_and_vars if
                                         grad is not None]
                # capped_grads_and_vars  = [(tf.math.tanh(grad), var) for grad, var in grads_and_vars if grad is not None]

                train_op = opt.apply_gradients(capped_grads_and_vars)

        return {
            "model_graph": model_graph,
            "source_input_data": source_input_data,
            "source_input_lengths": source_input_lengths,
            "target_input_data": target_input_data,
            "target_output_data": target_output_data,
            "target_output_lengths": target_output_lengths,
            "current_epoch": current_epoch,
            "current_learning_rate": current_learning_rate,
            "learning_rate": learning_rate,
            "keep_prob": keep_prob,
            "training_output": training_output,
            "inference_output": inference_output,
            "loss": loss,
            "train_op": train_op
        }

    elif mode == "predict":
        model_graph = tf.Graph()

        with tf.Session(graph=model_graph) as sess:
            saver = tf.train.import_meta_graph(model_path + '.meta')

            inference_output = model_graph.get_tensor_by_name("inference_output:0")
            source_input_data = model_graph.get_tensor_by_name("model_inputs/source_input_data:0")
            source_input_lengths = model_graph.get_tensor_by_name("model_inputs/source_input_lengths:0")
            #target_output_lengths = model_graph.get_tensor_by_name("model_inputs/target_output_lengths:0")
            keep_prob = model_graph.get_tensor_by_name('hyperparameters/keep_prob:0')
            return  {
                "saver": saver,
                "model_graph": model_graph,
                "inference_output": inference_output,
                "source_input_data": source_input_data,
                "source_input_lengths": source_input_lengths,
                #"target_output_lengths": target_output_lengths,
                "keep_prob": keep_prob
            }









