import re
import gensim
from gensim.models import Word2Vec
from gensim.models import KeyedVectors
from sklearn.decomposition import PCA
from matplotlib import pyplot
from collections import Counter

def load_CBOW_embedding(language):
    txt_model_file_path = "../../experiments/cbow/" + language.lower() + "_embedding.txt"
    model = KeyedVectors.load_word2vec_format(txt_model_file_path, binary=False)
    X = model[model.wv.vocab]
    result = model.most_similar(positive=['chết'], topn=3)
    print(result)
    result = model.similar_by_word("chết",topn=3)
    print(result)
    pca = PCA(n_components=2)
    result = pca.fit_transform(X)
    pyplot.scatter(result[:, 0], result[:, 1])
    words = list(model.wv.vocab)
    for i, word in enumerate(words):
        pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
    pyplot.show()


load_CBOW_embedding("vi")