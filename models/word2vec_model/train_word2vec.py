from gensim.models import Word2Vec
from collections import Counter
import itertools
import argparse
parser = argparse.ArgumentParser()

def split_text_into_tokens(text):
	return list(text.split())

def load_txt_file_into_list(pathfile):
    txt_file = open(pathfile, 'rt', encoding="utf-8")
    raw_text = txt_file.read().replace("\r", "\n").strip()
    list_of_lines = list(raw_text.split("\n"))
    txt_file.close()
    return list_of_lines

def load_text_corpus(language):
    if language == "en":
        sentence_list = load_txt_file_into_list('../../data/processed_data/embedding_corpus.en')
        tokenized_sentence_list = [split_text_into_tokens(sentence) for sentence in sentence_list]
    elif language == "vi":
        sentence_list = load_txt_file_into_list('../../data/processed_data/embedding_corpus.vi')
        tokenized_sentence_list = [split_text_into_tokens(sentence) for sentence in sentence_list]
    return tokenized_sentence_list

def generate_word2vec_embedding(language, w2v_model, window_size):
    w2v_model_filepath = "../../experiments/word_embeddings/word2vec/" + w2v_model + "/" + "window_size_" + str(window_size) + "/" + language.lower() + "_embedding.txt"

    tokenized_sentence_list = load_text_corpus(language)

    word_frequency_counter = Counter(list(itertools.chain.from_iterable(tokenized_sentence_list)))

    tokenized_sentence_list = list(map(lambda sentence: list(map(lambda token: token if word_frequency_counter[token] > 15 else "<UNK>", sentence)),  tokenized_sentence_list))

    model = Word2Vec(tokenized_sentence_list, min_count=1, sg=0 if w2v_model == "cbow" else 1, window=window_size, size=300, alpha=0.05, cbow_mean=1, iter=30)

    model.wv.init_sims(replace=True)

    model.wv.save_word2vec_format(w2v_model_filepath, fvocab=None, binary=False)

    print("Word2vec model has been successfully saved!")

if __name__ == '__main__':
    parser.add_argument('--w2v_model', default="cbow")
    args = parser.parse_args()
    if args.w2v_model in ["cbow", "skip_gram"]:
        for ws in [2, 5, 8]:
            for lg in ["en", "vi"]:
                generate_word2vec_embedding(language=lg, w2v_model = args.w2v_model, window_size = ws)