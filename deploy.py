'''Thực hiện load load model đã được huấn luyện đạt kết quả tốt nhất và thực hiện tạo bản dịch cho văn bản tiếng Anh'''

import argparse
import unicodedata
from pycorenlp import StanfordCoreNLP
from models.seq2seq_model.utils import load_json, print_params
from models.seq2seq_model.input_fn import get_model_data
from models.seq2seq_model.model_fn import get_model
from models.seq2seq_model.inference_fn import inference_fn

import tensorflow as tf
from tensorflow.contrib.seq2seq.python.ops import beam_search_ops

parser = argparse.ArgumentParser()
parser.add_argument('--best_model_dir', default='experiments/seq2seq/greedy_search/cbow/window_size_2/',
                    help="Directory containing best model and params")

args = parser.parse_args()

def tokenize_sentence(text):
    sentence_list = list()
    text = unicodedata.normalize("NFC", text)
    nlp = StanfordCoreNLP('http://localhost:9000')
    annotated = nlp.annotate(text, {'annotators': 'ssplit', 'outputFormat': 'json'})
    for sentence_object in annotated['sentences']:
        sentence = [obj['word'].replace("-- --","–").replace("--", "–").replace("– –", "–").replace("-LRB-","(").replace("-LCB-","{").replace("-LSB-","[").replace("-RRB-",")").replace("-RCB-","}").replace("-RSB-","]") for obj in sentence_object['tokens']]
        sentence = ' '.join(sentence).replace("``","\"").replace("''","\"").replace("`","'").strip()
        if sentence != "":
            sentence_list.append(sentence)

    return sentence_list

if __name__ == '__main__':
    best_model_dir = args.best_model_dir
    '''Lấy các hyperparameter của mô hình'''
    params_path = best_model_dir + 'params.json'
    best_model_path = best_model_dir + "model"
    params = load_json(params_path)
    print_params(params)

    '''Load các embedding, vocabulary,...'''
    model_data = get_model_data(params, mode="predict")

    '''Load mô hình tốt nhất sau quá quá trình huấn luyện'''
    best_model = get_model(model_data=None, params=params, mode="predict", model_path=best_model_path)
    model_graph = best_model["model_graph"]
    saver = best_model["saver"]
    with tf.Session(graph=model_graph) as sess:
        saver.restore(sess, best_model_path)
        while True:
            print("#########################################\n")
            en_text = input("Nhập đoạn văn bản tiếng Anh cần dịch:\n").strip()
            en_sentences = tokenize_sentence(en_text)
            translated_vi_sentences = inference_fn(sess=sess, en_sentences=en_sentences, model=best_model,
                                                       model_data=model_data, params=params)
            print("\n#########################################\nBản dịch tiếng Việt:\n")
            for sentence_in_tokens in translated_vi_sentences:
                print(" ".join(sentence_in_tokens))

