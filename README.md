# HỆ THỐNG DỊCH ANH-VIỆT DỰA TRÊN NEURAL NETWORKS

### 1. Mô tả
---------------------------------------------
- Đây là hệ thống dịch Anh-Việt dựa trên neural networks sử dụng mô hình Sequence-to-Sequence kết hợp với cơ chế attention của nhóm tác giả Lương Minh Thắng (Luong et al., 2015) do nhóm tự xây dựng lại
- Chứa các mô hình đã được huấn luyện trên các tập word embedding tạo bằng nhiều phương pháp khác nhau  
- Hệ thống có nhiệm vụ nhận vào đoạn văn bản tiếng Anh và thực hiện tính toán trả về bản dịch tiếng Việt tương ứng


### 2. Yêu cầu
---------------------------------------------
- Ngôn ngữ lập trình Python 3.6.5: https://www.python.org/downloads/release/python-365/
- Thư viện TensorFlow, phiên bản 1.14.0-rc1 trở lên: https://www.tensorflow.org/install
- Thư viện Gensim: https://radimrehurek.com/gensim/install.html
- Stanford CoreNLP web API server: http://nlp.stanford.edu/software/stanford-corenlp-full-2018-10-05.zip


### 3. Cấu trúc thư mục
---------------------------------------------
Cấu trúc thư mục code, dữ liệu và kết quả được chia như sau:<br/>
└── thesis_project<br/>
    ├── build_dataset<br/>
    │   ├── preprocess_data<br/>
    │   └── tmxio<br/>
    ├── data<br/>
    │   ├── raw_data<br/>
    │   ├── processed_data<br/>
    │   └── tmp<br/>
    ├── experiments<br/>
    │   ├── seq2seq<br/>
    │	└── word_embeddings<br/>
    ├── models<br/>
    │   ├── seq2seq_model<br/>
    │	└── word2vec_model<br/>
    ├── train.py<br/>
    ├── deploy.py<br/>
    └── plot_fn.py<br/>
	
Vai trò của các thư mục và tệp:
- build_dataset: chứa các module trên ngôn ngữ Python và project trên ngôn ngữ Java dùng để thu thập và thực hiện tiền xử lý cho các tập ngữ liệu
  + preprocess_data: chứa các hàm thực hiện công việc cho quá trình làm sạch và chuẩn hóa các tập ngữ liệu
  + tmxio: chứa các hàm hỗ trợ xử lý tệp dữ liệu có định dạng TMX
  
- data: chứa các tập ngữ liệu thô và các tập ngữ liệu đã được xử lý
  + raw_data: chứa dữ liệu thô chưa qua quá trình làm sạch và chuẩn hóa được nhóm thu thập 
  + processed_data: chứa dữ liệu đã được xử lý, bao gồm các tập ngữ liệu song ngữ và các tập ngữ liệu đơn ngữ
  + tmp: chứa các tệp dữ liệu nằm ở các bước trung gian trong quá trình xử lý làm sạch và chuẩn hóa
  
- experiments: chứa các kết quả thí nghiệm của các mô hình Sequence-to-Sequence kết hợp với cơ chế attention và các tập word embedding thu được từ các phương pháp khác nhau
  + seq2seq: chứa các kết quả thí nghiệm trên mô hình Sequence-to-Sequence
  + word_embeddings: chứa các tệp word embedding thu được từ các các phương pháp tạo word embedding khác nhau
  
- models: chứa mô hình Sequence-to-Sequence và các mô hình Word2Vec	
  + seq2seq_model: chứa các hàm xây dựng lại mô hình Sequence-to-Sequence kết hợp với cơ chế attention
  + word2vec_model: chứa các hàm huấn luyện tạo word embedding bằng các phương pháp Word2Vec
  
- train.py: gọi thực thi toàn bộ quá trình huấn luyện mô hình Sequence-to-Sequence kết hợp với cơ chế attention
  
- deploy.py: load mô hình Sequence-to-Sequence kết hợp với cơ chế attention đã được huấn luyện đạt kết quả tốt nhất và thực hiện tạo bản dịch cho văn bản tiếng Anh  

- plot_fn.py: vẽ đồ thị đường so sánh điểm BLEU trên tập development giữa các mô hình Sequence-to-Sequence kết hợp với cơ chế attention khi sử dụng các tập word embedding 
  được tạo bằng các phương pháp khác nhau trong quá trình huấn luyện

#### 3.1 Thư mục build_dataset
---------------------------------------------  
Cấu trúc thư mục models được chia như sau:<br>
└── build_dataset<br/>
    ├── preprocess_data<br/>
    │   ├── data_analysis.py<br/>
    │   ├── merge_all_envi_data.py<br/>
    │   ├── merge_embedding_corpus.py<br/>
    │   ├── normalize_baomoi_data.py<br/>
    │   ├── normalize_cnn_data.py<br/>
    │   ├── normalize_data_formats.py<br/>
    │   ├── normalize_evbnews_data.py<br/>
    │   ├── normalize_guardian_data.py<br/>
    │   ├── normalize_iwslt_data.py<br/>
    │   ├── normalize_qt21_data.py<br/>
    │   └── normalize_vnexpress_data.py<br/>
    └── tmxio<br/>
        └── tmx_file_io.py<br/>

- preprocess_data: 
  + data_analysis.py: chứa các hàm thống kê số lượng token và số lượng câu của các tập ngữ liệu
  + merge_all_envi_data.py: chứa các hàm dùng để tổng hợp các tập ngữ liệu song ngữ Anh-Việt
  + merge_embedding_corpus.py: chứa các hàm dùng để tổng hợp các tập ngữ liệu đơn ngữ tiếng Anh và tập ngữ liệu đơn ngữ tiếng Việt
  + normalize_baomoi_data.py: chứa các hàm dùng để thực hiện tiền xử lý cho tập ngữ liệu thu được từ trang tin điện tử https://baomoi.com
  + normalize_cnn_data.py: chứa các hàm dùng để thực hiện tiền xử lý cho tập ngữ liệu thu được từ trang tin điện tử https://edition.cnn.com
  + normalize_data_formats.py: chứa các hàm thực hiện chuẩn hóa thống nhất định dạng cho các tập ngữ liệu khác nhau về một định dạng chung để dễ dàng thao tác xử lý
  + normalize_evbnews_data.py: chứa các hàm dùng để thực hiện tiền xử lý cho tập ngữ liệu EVBNews
  + normalize_guardian_data.py: chứa các hàm dùng để thực hiện tiền xử lý cho tập ngữ liệu thu được từ trang tin điện tử https://www.guardian.com
  + normalize_iwslt_data.py: chứa các hàm dùng để thực hiện tiền xử lý cho các tập ngữ liệu IWSLT English-Vietnamese
  + normalize_qt21_data.py: chứa các hàm dùng để thực hiện tiền xử lý cho tập ngữ liệu QT21
  + normalize_vnexpress_data.py: chứa các hàm dùng để thực hiện tiền xử lý cho tập ngữ liệu thu được từ trang tin điện tử https://vnexpress.net

- tmxio
  + tmx_file_io.py: chứa các hàm dùng để xử lý thao tác trên tệp dữ liệu có định dạng TMX
  
#### 3.2 Thư mục experiments
---------------------------------------------

##### 3.2.1 Thư mục seq2seq
---------------------------------------------<br/>
└── experiments<br/>
    ├── seq2seq<br/>
    │   ├── beam_search<br/>
    │	│   ├── gaussian_random<br/>
    │	│   │   ├── window_size_2<br/>
    │	│   │   └── ...<br/>
    │	│   ├── cbow<br/>
    │	│   │   ├── window_size_2<br/>
    │	│   │   └── ...<br/>
    │	│   ├── skip_gram<br/>
    │	│   │   ├── window_size_2<br/>
    │	│   │   └── ...<br/>
    │	│   ├── glove<br/>
    │	│   │   ├── window_size_2<br/>
    │	│   │   └── ...<br/>
    │   └── greedy_search<br/>
    │	    ├── gaussian_random<br/>
    │	    │   ├── window_size_2<br/>
    │	    │   └── ...<br/>
    │	    ├── cbow<br>
    │	    │   ├── window_size_2<br/>
    │	    │   └── ...<br/>
    │	    ├── skip_gram<br/>
    │	    │   ├── window_size_2<br/>
    │	    │   └── ...<br/>
    │	    └── glove<br/>
    │	        ├── window_size_2<br/>
    │	        └── ...<br/>
    └── word_embeddings<br/>

- Chia ra 2 thư mục con tương ứng với 2 thuật toán giải mã của mô hình Sequence-to-Sequence kết hợp với cơ chế attention: 
  + beam_search 
  + greedy_search
  
- Mỗi thư mục thuật toán giải mã sẽ có 4 thư mục con tương ứng với các phương pháp tạo word embedding:
  + gaussian_random
  + cbow
  + skip_gram
  + glove
  
- Mỗi thư mục tương ứng với mỗi phương pháp tạo word embedding sẽ có các thư mục con tương ứng với các giá trị khác nhau của window size:   			
  + window_size_2
  + window_size_5
  + window_size_8
  
- Mỗi thư mục window_size ban đầu chỉ chứa 1 tệp params.json chứa thông tin cấu hình để huấn luyện mô hình.
  Sau quá trình huấn luyện, thư mục sẽ có nội dung như sau:<br/>
  └── window_size<br/>
&nbsp;├── tensorboard<br/>
&nbsp;├── checkpoint<br/>
&nbsp;├── model.data-00000-of-00001<br/>
&nbsp;├── model.index<br/>
&nbsp;├── model.meta<br/>
&nbsp;├── params.json<br/>
&nbsp;├── test_data_translations.txt<br/>
&nbsp;└── training_log.txt<br/>

  + tensorboard: thư mục chứa tệp tin lưu trữ một số thông tin từ công cụ TensorBoard, chủ yếu dùng để trực quan hóa quá trình huấn luyện
  + checkpoint, model.data-00000-of-00001, model.index, model.meta: các tệp checkpoint chứa thông tin của mô hình được tạo ra và lưu lại bằng thư viện TensorFlow  
  + params.json: chứa các tham số và hyperparameter được cấu hình để huấn luyện mô hình
  + test_data_translations.txt: tệp này chứa bản dịch tiếng Việt của các câu tiếng Anh từ mô hình trên tập test
  + training_log.txt: tệp này chứa các thông tin trong quá trình huấn luyện mô hình, ghi lại các kết quả điểm BLEU của mô hình trên tập development và tập test tại một số bước huấn luyện nhất định 	
    
##### 3.2.2 Thư mục word_embeddings
---------------------------------------------  
└── experiments<br/>
    ├── seq2seq<br/>
    ├── word_embeddings<br/>
    │   ├── gaussian_random<br/>
	│   │   ├── standard_deviation_0.05<br/>
	│   │   └── ...<br/>
    ├── ├── glove<br/>
	│   │   ├── window_size_2<br/>
	│   │   └── ...<br/>
    └── word2vec<br/>
	    ├── cbow<br/>
	    │   ├── window_size_2<br/>
	    │   └── ...<br/>
	    └── skip_gram<br/>
	        ├── window_size_2<br/>
	        └── ...<br/>

- Chia ra 3 thư mục con cho các phương pháp tạo word embedding khác nhau:
  + gaussian_random
  + glove
  + word2vec
  
- Thư mục gaussian_random sẽ có các thư mục con tương ứng với các giá trị khác nhau của phương sai trong phân phối Gauss:  
  +  standard_deviation_0.05
  +  standard_deviation_0.5
  +  standard_deviation_1.0
  
- Thư mục word2vec sẽ có các thư mục con tương ứng với các mô hình word2vec:   
  + cbow
  + skip_gram

- Mỗi thư mục mô hình word2vec và mô hình glove sẽ có các thư mục con tương ứng với các giá trị khác nhau của hyperparameter window_size:   
  + window_size_2
  + window_size_5
  + window_size_8   

- Mỗi thư mục standard_deviation và window_size sẽ có nội dung như sau:<br/>
  └── window_size<br/>
	  ├── en_embedding.txt<br/>
	  └── vi_embedding.txt<br/>

  + en_embedding.txt: tệp tin chứa dữ liệu word embedding cho ngôn ngữ tiếng Anh	  
  + vi_embedding.txt: tệp tin chứa dữ liệu word embedding cho ngôn ngữ tiếng Việt	    
  
#### 3.3 Thư mục models
---------------------------------------------
Cấu trúc thư mục models được chia như sau:<br/>
└── models<br/>
    ├── seq2seq_model<br/>
    │   ├── input_fn.py<br/>
    │   ├── model_fn.py<br/>
    │   ├── training.py<br/>
    │   ├── inference_fn.py<br/>
    │   ├── utils.py<br/>
    └── word2vec_model<br/>
    	└── train_word2vec.py<br/>

- seq2seq_model:
  + input_fn.py: chứa các hàm xử lý các dữ liệu đầu vào cần thiết cho quá trình huấn luyện
  + model_fn.py: chứa các hàm xây dựng lại mô hình Sequence-to-Sequence kết hợp với cơ chế attention của nhóm tác giả Lương Minh Thắng
  + training.py: chứa các hàm huấn luyện và đánh giá mô hình đã xây dựng trên các tập dữ liệu được cung cấp
  + inference_fn.py: chứa các hàm thực hiện quá trình tạo bản dịch từ văn bản tiếng Anh sang tiếng Việt và hậu xử lý văn bản
  + utils.py: chứa các hàm được các module khác gọi lại và chứa các hàm tạo tập word embedding bằng phương pháp Gaussian random
  
- word2vec_model:
  + train_word2vec.py: huấn luyện mô hình word2vec để tạo ra tệp word embedding

  
### 4. Cách sử dụng
---------------------------------------------

#### 4.1 Huấn luyện mô hình
---------------------------------------------
- Gọi hàm trong module Train của hệ thống dịch máy, "--model_dir" là đường dẫn tới thư mục có tệp params.json chứa các tham số được thiết lập để huấn luyện mô hình
> python3 train.py --model_dir=experiments/seq2seq/beam_search/cbow/window_size_8/

#### 4.2 Triển khai hệ thống dịch máy
---------------------------------------------
- Tạo Stanford CoreNLP web API server:
> java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -port 9000 -timeout 15000

- Gọi hàm trong module Deploy của hệ thống dịch máy, trong đó tham số "best_model_dir" là đường dẫn tới mô hình đã được huấn luyện đạt kết quả tốt nhất:
> python3 deploy.py --best_model_dir=experiments/seq2seq/beam_search/cbow/window_size_8/


### 5. Tài liệu tham khảo
---------------------------------------------
- M. Luong, H. Pham, and C. D. Manning, "Effective approaches to attention-based
neural machine translation" CoRR, vol. abs/1508.04025, 2015. [Online]. Available:
http://arxiv.org/abs/1508.04025
