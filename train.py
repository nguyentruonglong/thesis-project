'''Thực hiện load các hyperparameter, load dữ liệu, model để train và evaluate'''

import argparse
from models.seq2seq_model.utils import load_json, print_params
from models.seq2seq_model.input_fn import get_model_data
from models.seq2seq_model.model_fn import get_model
from models.seq2seq_model.training import train_and_evaluate

parser = argparse.ArgumentParser()
parser.add_argument('--model_dir', default='experiments/seq2seq/greedy_search/cbow/window_size_2/',
                    help="Directory containing model and params")

args = parser.parse_args()

if __name__ == '__main__':
    params_path = args.model_dir + "params.json"
    model_path = args.model_dir + "model"
    '''Lấy các hyperparameter của mô hình'''
    params = load_json(params_path)
    print_params(params)

    '''Lấy các tập dữ liệu train, dev, test, word embedding, vocabulary,... đã được xử lý'''
    model_data = get_model_data(params, mode="train_and_eval")

    '''Load graph của mô hình đã được xây dựng'''
    train_model = get_model(model_data, params, mode="train_and_eval", model_path=model_path)

    '''Huấn luyện và đánh giá mô hình'''
    train_and_evaluate(train_model, model_data, params, model_path)
